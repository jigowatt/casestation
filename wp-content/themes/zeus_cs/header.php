<?php
if(function_exists('cs_log')) {
 cs_log('LOADED header.php');
}
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, maximum-scale=1.0" />

	<!-- page title -->
	<title><?php wp_title() ?></title>

  <!-- icon -->
	<link rel="icon" type="image/png" href="https://casestation.com/img/favicon.png">

	<?php wp_head(); ?>

</head>

<body <?php body_class('no-js') ?>>

	<!-- let CSS know JS is enabled -->
	<!-- inlined for performance -->
	<script>document.body.className = document.body.className.replace("no-js","has-js");</script>

  <?php do_action('cs_body_top') ?>

	<?php get_template_part(PATH_PARTIALS.'block', 'header_site-notices'); ?>

	<?php get_template_part(PATH_PARTIALS.'block', 'header_branding-bar--primary'); ?>

	<?php get_template_part(PATH_PARTIALS.'block', 'header_branding-bar--secondary'); ?>

	<?php get_template_part(PATH_PARTIALS.'block', 'header_mobile-nav'); ?>

	<div class="page-wrapper">


		<div class="main-content">

			<?php if (cs_contain_page()): ?>
			<div class="contain">
			<?php endif; ?>
