��    _                      	            .        L     `     h     v     z     �     �  :   �     �     �     	     	     2	     >	     K	     g	  
   y	  A   �	  E   �	     
     
     )
  9   E
     
     �
      �
     �
     �
  e   �
     A     J     X     _     e     y     �     �     �     �  *   �     �     �     �     �  "        /  	   8     B     J     ]  
   l     w     �     �     �  G   �     �               '     6     G     Y     s     y     �  8   �  )   �     �               +     >     G     Y     s  	   �     �  U   �  @        G  %   P     v  
   �  	   �  
   �     �     �     �  $   �  �       �     �     �  .   �     �          	               $     ;  :   G     �     �     �     �     �     �     �       
     A   %  E   g     �     �     �  9   �           -      ?     `     h  e   |     �     �     �                     '     /     4     E  *   R     }     �     �     �  "   �     �  	   �     �     �     �  
             (     9     O  H   X     �     �     �     �     �     �     �               -  8   >  )   w     �     �     �     �     �     �     �       	   0     :  U   R  @   �     �  %   �       
   /  	   :  
   D     O     b     y     �           <   J      8      A   Z   &   #   H   ?           V                 
   4      :      C          3   O          ^      S   G             W   7   E           U   B   *   P       ;       ]   6   Q             .       K   	   %          R       )       9   ,   +   M          5   '          I              /   @   (              F          !   T   0           D   X                       2       \   "   $          N   [       >          1   _       -   Y   L          =    %d %d &larr; &rarr; A password will be sent to your email address. Accessories Filters Account Add to basket All Archives Awaiting product image Buy Product CS Labs Quality Tested | Made for life, lifetime guarantee Case designer product slug: Change region Choose Your Region Collections Filters Coming soon Coming soon? Comments are closed, sorry. Confirm Password* Create now Display different "Add to cart" button text (in-stock items only) Display different "Add to cart" button text (out-of-stock items only) Email address Email address* Enter a new password below. Enter the slug for the personalise-IT product to link to. Footer Links Footer Newsletter Free Shipping - SHIPPED SAME DAY Gallery Go to the homepage. If your search results show the wrong device model, try putting quote marks around what you searched! Jigowatt Keep in touch Log in Login Lost your password? Main sidebar May 4th Meta Mobile Menu Area New password No case designer available for your choice Orders Other people's views Page Page not found Parent theme for Casestation: .com Password Password* Pin it! Pre-order - Ships  Pre-order Date Pre-order? Product Details Protection level Re-enter new password Register Register <small>10&#37; off your first order for new customers!</small> Remember me Save Search Case Station… Search for art Search results:  Search results… Select another case style Share Share on facebook Share on twitter Shows "Coming soon" button (must be set to out-of-stock) Shows pre-order button (must be in-stock) Sign in Sign out Use Amazon Pay at checkout Useful Information Username Username or email Username or email address Username or email address* Username* View your shopping cart We recommend you visit your local site for the most relevant content and information. We were unable to find anything matching your search, try again. Wishlist You seem to have stumbled off course. Your Payment is Secure Your email Your name Your words case-designer-slug https://jigowatt.co.uk submit buttonSearch submit buttonSearch Case Station… Project-Id-Version: ⚡️ Zeus
PO-Revision-Date: 2020-09-01 16:10+0100
Last-Translator: 
Language-Team: 
Language: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.1
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: style.css
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 %d %d &larr; &rarr; A password will be sent to your email address. Accessories Filters Account Add to basket All Archives Awaiting product image Buy Product CS Labs Quality Tested | Made for life, lifetime guarantee Case designer product slug: Change region Choose Your Region Collections Filters Coming soon Coming soon? Comments are closed, sorry. Confirm Password* Create now Display different "Add to cart" button text (in-stock items only) Display different "Add to cart" button text (out-of-stock items only) Email address Email address* Enter a new password below. Enter the slug for the personalise-IT product to link to. Footer Links Footer Newsletter Free Shipping - SHIPPED SAME DAY Gallery Go to the homepage. If your search results show the wrong device model, try putting quote marks around what you searched! Jigowatt Keep in touch Log in Login Lost your password? Main sidebar May 4th Meta Mobile Menu Area New password No case designer available for your choice Orders Other people's views Page Page not found Parent theme for Casestation: .com Password Password* Pin it! Pre-order - Ships  Pre-order Date Pre-order? Product Details Protection level Re-enter new password Register Register <small>&pound;5 off your first order for new customers!</small> Remember me Save Search Case Station… Search for art Search results:  Search results… Select another case style Share Share on facebook Share on twitter Shows "Coming soon" button (must be set to out-of-stock) Shows pre-order button (must be in-stock) Sign in Sign out Use Amazon Pay at checkout Useful Information Username Username or email Username or email address Username or email address* Username* View your shopping cart We recommend you visit your local site for the most relevant content and information. We were unable to find anything matching your search, try again. Wishlist You seem to have stumbled off course. Your Payment is Secure Your email Your name Your words case-designer-slug https://jigowatt.co.uk Search Search Case Station… 