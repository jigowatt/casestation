import helper from './theme-helper.js';

/**
 * This file deals with all the menu/sub-menu behaviour of the theme
 * @type {[type]}
 */
(function($){

  /**
   * Centrally aligns a second level sub-menu based on parent menu item
   * @param  {object} subnav .sub-menu jQuery object to centre
   */
  function centreAlignSubMenu(subnav) {
    subnav.show(); // needed for JS to calculate stuff
    const winWidth = window.innerWidth;
    const parent = subnav.parent();
    const parentMargin = parseInt(parent.css('padding-left').replace('px', ''));
    const parentWidth = parent.outerWidth();
    const subnavWidth = subnav.outerWidth();
    const subnavOffset = -((subnavWidth + parentMargin) / 2) + (parentWidth/2);

    // centre align based on parent
    subnav.css('margin-left', subnavOffset);

    // if the sub-menu now collides with the window bounds, re-adjust to middle of window
    const collided = detectWindowCollision(subnav);
    if(collided || parent.is('.wide')){
      subnav.css('margin-left','');
      const currentOffset = subnav.offset().left;
      const correctOffset = (winWidth - subnavWidth) / 2;
      const difference = currentOffset - correctOffset;
      subnav.css('margin-left', -difference);
    }

    // turn off display block
    subnav.css('display', '');
  }

  /**
   * Detects if the sub-menu goes outside of the window bounds
   * @param  {object} subnav .sub-menu jQuery object
   * @return {bool}        collision detection result
   */
  function detectWindowCollision(subnav) {
    const winWidth  = window.innerWidth;
    const subOffset = subnav.offset().left;
    const subWidth  = subnav.outerWidth();
    return (subOffset+subWidth > winWidth) ? true : false ;
  }

  /**
   * Re-aligns .sub-menu .sub-menu elements when it goes outside of the window bounds
   * @param  {object} subnav .sub-menu jQuery object
   */
  function alignSlimSubSubMenu(subnav) {
    subnav.show();
    const subSubnavs  = subnav.find('.sub-menu');
    const collision   = detectWindowCollision(subSubnavs);
    const subnavWidth = subnav.outerWidth();
    if (collision) { subSubnavs.css('left', -subnavWidth); }
    subnav.css('display', '');
  }

  /**
   * Used to format "slim" (one column) sub-menus
   */
  function formatSlimMenus() {
    $('.branding-bar nav > ul > li:not(.wide) > .sub-menu').each(function(){
      centreAlignSubMenu($(this));
      if($(this).children('.sub-menu').length){
        $(this).on('hover', function(){
          alignSlimSubSubMenu($(this));
        });
      }
    });
  }

  /**
  * Makes sure the .sub-menu fits in the current window based on column width
  * @param  {object} subnav .sub-menu jQuery object
  */
  function setWideSubMenuWidth(subnav) {
    subnav.show();
    const winWidth = window.innerWidth;
    const columns = subnav.find('> li').length;
    const colWidth = subnav.find('> li').show().outerWidth(); subnav.find('> li').css('display', '');
    const subPadding = parseFloat(subnav.css('padding-left').replace('px', '')) + parseFloat(subnav.css('padding-right').replace('px', ''));
    const subBorder = parseFloat(subnav.css('border-left-width').replace('px', '')) + parseFloat(subnav.css('border-right-width').replace('px', ''));
    const trueWidth = (colWidth * columns) + (subPadding + subBorder);
    const subWidth = (trueWidth > winWidth) ? (Math.floor((winWidth-(subPadding + subBorder)) / colWidth) * colWidth) : trueWidth - (subPadding + subBorder);
    subnav.css('width', subWidth);
    subnav.css('display', '');
  }

  /**
   * Used to format "wide" (multi-column) sub-menus
   */
  function formatWideSubMenus() {
    $('.branding-bar nav > ul > li.wide > .sub-menu').each(function(){
      setWideSubMenuWidth($(this));
      centreAlignSubMenu($(this));
    });
  }

  /**
   * Toggle secondary branding bar
   */
  function toggleSecondaryBrandingBar() {
    if(!$('body').is('.cs-device-category')){
      const trigger = $('.branding-bar--primary').css('height').replace('px', '');
      const offset  = ($('#wpadminbar').css('position') === 'fixed') ? ($('.branding-bar--secondary').outerHeight() + $('#wpadminbar').outerHeight()) : $('.branding-bar--secondary').outerHeight();
      const style = (window.scrollY > trigger || $('.branding-bar--primary:hidden').length) ? 'transform: translateY('+offset+'px); opacity: 1;' : '';
      $('.branding-bar--secondary').attr('style', style);
    }
  }


  /**
   * Apply desktop navigation behaviour
   */
  $(document).ready(function(){
    formatSlimMenus();
    formatWideSubMenus();
  });
  $(window).resize(function(){
    formatSlimMenus();
    formatWideSubMenus();
  });


  /**
   * Language Switcher
   */
  $(document).ready(function(){
    $('.current-lang, .lang-chooser .close').click(function(e){
      e.preventDefault();
      $('body').removeClass('show-mobile-menu').toggleClass('show-lang-chooser');
    });
  });

  /**
   * Functio to show/hide mobile menu
   */
  function showHideMobileMenu(){
    const $body = $('body');
    const showMobileClassName = 'show-mobile-menu';
    const hideMobileClassName = 'hide-mobile-menu';
    const delay = 600;

    // set the height of the menu to the document height for scrolling purposes
    const scrollerHeight = window.innerHeight - $('.mobile-utility').outerHeight();
    $('.mobile-nav-container .scroller, .mobile-nav-container .sub-menu').css('height', 'calc('+scrollerHeight+'px - 5rem)');

    // toggle the mobile menu on/off
    if ($body.hasClass(showMobileClassName) && !$body.hasClass(hideMobileClassName)) {
      $body.removeClass(showMobileClassName).addClass(hideMobileClassName);
      setTimeout(function(){$body.removeClass(hideMobileClassName);}, delay);
      $('html').css('overflow', 'visible');
    } else {
      $body.addClass(showMobileClassName);
      toggleSecondaryBrandingBar();
      $('html').css('overflow', 'hidden');
    }
  }


  /**
   * Mobile Menu/Burger Button
   */
  $(document).ready(function() {
    $('.burger').click(showHideMobileMenu);
  });


  /**
   * Mobile menu behaviour 
   */
  $(document).ready(function(){

    // add close link
    $('<a class="close">&times;</a>')
      .click(showHideMobileMenu)
      .prependTo('.mobile-nav-container');

    // add sub-menu title container to populate with currently viewed sub-menu title
    $('<span class="sub-menu-title"></span>').prependTo('.mobile-nav-container');

    // add back button to dismiss sub-menus
    $('<a class="back">←</a>')
      .click(function(){
        if($('.sub-menu.active').length){
          const last_sub = $('.sub-menu.active:last');

          // remove the "active" class from the currently expanded sub-menu
          last_sub.removeClass('active');

          // reset the sub-menu-title to the relevant text
          $('.sub-menu-title').text(last_sub.parent().parent().parent().children('a').text());
          
          // if the last sub-menu has been deactivated, fade back button
          if(!$('.sub-menu.active').length) {
            $('.mobile-nav-container .back').removeClass('active');
          }
        } else {
          $('.sub-menu-title').text('');
        }
      })
      .prependTo('.mobile-nav-container');
    
    // add click event to menu links
    $('#menu-mobile-menu a').click(function(e){

      const link = $(this);
      const has_sub = link.next().is('.sub-menu');

      // if the clicked link has a sub-menu, we should show it
      if(has_sub){
        e.preventDefault(); // prevent click default behaviour
        link.next().addClass('active'); // set sub-menu active
        $('.sub-menu-title').text(link.text());
        $('.mobile-nav-container .back').addClass('active');
      }

    });

  });



  /**
   * Scroll event
   */
  $(window).on('scroll', function(){

    if (helper.touch.enabled) {
      setTimeout(function(){
        toggleSecondaryBrandingBar();
      }, 750);
    } else {
      toggleSecondaryBrandingBar();
    }
  });


})(jQuery);
