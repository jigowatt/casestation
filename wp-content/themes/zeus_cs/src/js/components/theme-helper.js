/**!
 * THEME HELPER OBJECT
 * This is useful for storing theme-wide util stuff
 */

// helper object to store it all
let helper = {};

// viewport
helper.viewport = {
	width :      function(){ return Math.max(document.documentElement.clientWidth, window.innerWidth || 0); },
	height :     function(){ return Math.max(document.documentElement.clientHeight, window.innerHeight || 0); },
};

// breakpoints
helper.breakpoint            = {};
helper.breakpoint.isPortable = function(){return (helper.viewport.width() <= 1024) ? true : false; };
helper.breakpoint.isPalm     = function(){return (helper.viewport.width() <= 568) ? true : false; };
helper.breakpoint.isLap      = function(){return (helper.viewport.width() >= 569 && helper.viewport.width() <= 1024) ? true : false; };
helper.breakpoint.isLapAndUp = function(){return (helper.viewport.width() >= 569) ? true : false; };
helper.breakpoint.isDesk     = function(){return (helper.viewport.width() >= 1025) ? true : false; };
helper.breakpoint.isDeskWide = function(){return (helper.viewport.width() >= 1200) ? true : false; };

// paths (phpVars is provided by functions.php - assigning to theme obj to keep central)
helper.path = document.phpVars;

// console debugging
helper.log = function(message) {
	if( window.console && console.log ) {
		console.log(message);
	}
};
helper.warn = function(message) {
	if( window.console && console.warn ) {
		console.warn(message);
	}
};
helper.info = function(message) {
	if( window.console && console.info ) {
		console.info(message);
	}
};
helper.dir = function(thing) {
	if( window.console && console.dir ) {
		console.dir(thing);
	}
};

// touch event helpers
helper.touch = {
	enabled : ('ontouchstart' in window || navigator.msMaxTouchPoints),
	on : false,
	swipe : false
};
jQuery(window).on('touchstart', function(){	helper.touch.on = true; helper.log('touch started'); });
jQuery(window).on('touchmove', function(){	helper.touch.swipe = true; helper.log('swiping'); });
jQuery(window).on('touchend', function(){	helper.touch.on = false; helper.touch.swipe = false; console.log('touch/swipe stopped'); });

// kick the module out
export default helper;
