import helper from './theme-helper.js';

(function($){

  // for some reason this is needed for WooCommerce variations to work correctly with select2
  // perhaps it needs to be run after all the javascript in the page has fully loaded (Woo JS)
  $(window).bind("load", function() { $('select').select2(); });

  $(window).on("load", function() {
    // work around for woocommerce's single-product.js not accounting for multiple tabbed areas
    $('.woocommerce-tabs .woocommerce-Tabs-panel:first-of-type').show(); // show first panel of each tab set

    // conditionally add variation change event handler to change add_to_cart button
    $('.variations_form').on('show_variation', function(event, variation, is_purchasable){
      const button = $(this).find('button[type=submit]');

      let soon;
      let preorder;
      let ec3d_pod_ref;
      let ec3d_product_ref;

      // loop through variation meta data and assign values where populated
      if(variation.meta_data){        
        for (var i = 0; i < variation.meta_data.length; i++) {
          const meta = variation.meta_data[i];

          // get POD ref value
          if('woocommerce_ec3d_print_on_demand' === meta.key && meta.value !== ''){
            ec3d_pod_ref = meta.value;
          }
          
          // get product ref value
          if('woocommerce_ec3d_product_ref' === meta.key && meta.value !== ''){ 
            ec3d_product_ref = meta.value;
          }

          // if coming soon is set to "yes", store the value
          if ('_coming_soon' === meta.key && 'yes' === meta.value) {
            soon = meta.value;
          }

          // if preoder has a value, store the value
          if ('_preorder' === meta.key && meta.value) {
            preorder = meta.value;
          }
        }
      }
      // make sure we're only changing the button on products with a POD or EC3D product reference set

      if(ec3d_pod_ref || ec3d_product_ref){

        // if the product is set to "out of stock", and "coming soon" is checked, change button
        if (!is_purchasable && soon) {
          button.text(phpVars.comingsoon_txt);
        }

        // if the products is "in stock", and pre-order field has a value, change button
        else if (is_purchasable && preorder){
          button.text(phpVars.preorder_txt+preorder);
        }

        // otherwise, kick out the default text
        else {
          if(ec3d_product_ref){
            button.text(phpVars.default_designer_txt); // for items which forward to a "designer" product
          } else {
            button.text(phpVars.default_txt); // for items which are "stock" (no "designer")
          }
        }

      }

    }).find('select').trigger('change');

  });

  // refresh cart fragments on wishlist addition and removal
  $(document).on('added_to_wishlist removed_from_wishlist', function(){
      $( document.body ).trigger( 'wc_fragment_refresh' );
  });

  // sticky device category case type menu
  $(window).on('scroll', function(){

    // if we're not on mobile (screen real estate at premium) and the secondary nav has kicked in
    if(window.scrollY > ($('.page-wrapper').offset().top)){

      // set header as sticky
      $('.case-type-menu-container').css({
        position : 'fixed',
        top : '0',
        marginTop: '0',
        width: '100%',
        zIndex: 10
      });
    } else {
      $('.case-type-menu-container').removeAttr('style');
    }
  });

})(jQuery);
