import helper from './components/theme-helper.js';
import './components/navigation.js'; // navigation behaviour
import './components/woocommerce.js'; // woocommerce behaviour

function setNoticeCookie(cname, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + ( (1000*60*60*24) * exdays) ) ;
  var expires = "expires="+ d.toUTCString();
  document.cookie = cname+ "=yes;" + expires + ";path=/";
}

(function($){

  /**
   * Initialises waypoints using a selector and classes passed in
   */ 
  function init_js_animation(elem, classes = 'animated fadeIn'){
    $(elem).on('inview', function() { 
      $(this).addClass(classes); 
    });
  }


  /**
   * Pins the footer to the bottom of the window if content isn't tall enough to force it to the bottom.
   */
  function pinFooter(){
    const footer = $('.main-footer');
    let footerBottomEdge = (footer.data('bottom-edge')) ? footer.data('bottom-edge') : null;

    if(!footerBottomEdge){
      footerBottomEdge = footer.offset().top + footer.outerHeight();
      footer.data('bottom-edge', footerBottomEdge);
    }

    if(footerBottomEdge < window.innerHeight){
      footer.addClass('pin');
    } else {
      footer.removeClass('pin');
    }
  }

  /**
   * PAGE LOAD (all pages)
   */
  $('document').ready(function(){

    // add select2 styling to dropdowns
    if(jQuery().select2){ $('select').select2(); }

    // image links "in view" animations
    init_js_animation('.image-links__item');

    // collabs grid "in view" animations
    init_js_animation('.collab_grid__item');

    // two graphics "in view" animations
    init_js_animation('.two_graphics .images-item');

    // other (simple) "in view" animations
    init_js_animation('.full_width_graphic_text_overlay .grid__item', 'animated fadeInDown');
    
    // product inview animations
    init_js_animation('.products .product');

    // trigger the scroll event to force items already in view to show
    $(window).scroll();

    // hide labels on gravityform fields where there's a placeholder attribute set
    const fields = `input[type!=radio][placeholder],
                    input[type!=checkbox][placeholder],
                    input[type!=button][placeholder],
                    input[type!=submit][placeholder],
                    .gf_placeholder:parent`;
    $(fields).closest('.gfield').children('label').addClass('has-placeholder');

    // add high-resolution (full size) imagery on larger screens
    if(helper.breakpoint.isDesk()){
      $(".full_width_graphic_text_overlay [style*='background-image']").each(function(){
        const rex = /-[0-9]+x[0-9]+/i;
        let backgroundimg = $(this).css('background-image');
        backgroundimg = backgroundimg.replace(rex, '');
        $(this).css('background-image', backgroundimg);
      });
    }

    // add close button to site notices to be able to dismiss them.
    $('.cs-notice').each(function(){
      $('<span class="close">&#10005;</span>')
        .click(function(){
          setNoticeCookie($(this).parent().parent().attr('id'), 1);
          $(this).parent().parent().slideUp('slow');
        })
        .prependTo($(this).find('.contain'));
    });
  
    // position footer at bottom of window if content doesn't fill it
    pinFooter();

    // add click event to search form trigger
    $('body').addClass('hide-search');
    $('.icon--search').on('click', function(e){
      e.preventDefault();
      
      // if search isn't being shown, then show it
      if($('body').hasClass('hide-search')) {
        $('body').removeClass('hide-search').addClass('show-search');
        $('.branding-bar__search #s').focus();
      } else {
        $('body').removeClass('show-search').addClass('hide-search');
      }
    });

  });

  $(window).resize(function(){
    pinFooter();
  });

})(jQuery);
