<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>404</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta name="description" content="Personalise your case - iPhone 6s, Apple Watch band, Galaxy S6 Galaxy S6 Edge. Create your personalized case with Case Station!"/>
		<meta name="robots" content="noindex, nofollow"/>
		<link rel="canonical" href="https://casestation.com/" />
		<meta property="og:locale" content="en_US" />
		<meta property="og:type" content="website" />
		<meta property="og:title" content="Personalise your case - Samsung Galaxy S7, S6, iPhone 5se, 6s and Apple Watch band" />
		<meta property="og:description" content="Personalise your case - iPhone 6s, Apple Watch band, Galaxy S6 Galaxy S6 Edge. Create your personalized case with Case Station!" />
		<meta property="og:url" content="https://casestation.com/" />
		<meta property="og:site_name" content="Case Station UK" />
		<meta property="og:image" content="https://casestation.com/img/HeroTest5.jpg" />
		<meta name="twitter:card" content="summary"/>
		<meta name="twitter:description" content="Personalise your case - iPhone 6s, Apple Watch band, Galaxy S6 Galaxy S6 Edge. Create your personalized case with Case Station!"/>
		<meta name="twitter:title" content="Personalise your case - Samsung Galaxy S7, S6, iPhone 5se, 6s and Apple Watch band"/>
		<meta name="twitter:image" content="https://casestation.com/img/HeroTest5.jpg"/>
		<meta name="msvalidate.01" content="EE497D94CACE6CCA8893B798B6DB947E" />
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans%3A300italic%2C400italic%2C600italic%2C300%2C400%2C600&#038;subset=latin%2Clatin-ext&#038;ver=4.3.1">
		<link rel="icon" type="image/png" href="https://casestation.com/img/favicon.png">

		<!-- normalize css -->
		<style>/*! normalize.css v4.1.1 | MIT License | github.com/necolas/normalize.css */progress,sub,sup{vertical-align:baseline}button,hr,input{overflow:visible}[type=checkbox],[type=radio],legend{box-sizing:border-box;padding:0}html{font-family:sans-serif;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}body{margin:0}article,aside,details,figcaption,figure,footer,header,main,menu,nav,section,summary{display:block}audio,canvas,progress,video{display:inline-block}audio:not([controls]){display:none;height:0}[hidden],template{display:none}a{background-color:transparent;-webkit-text-decoration-skip:objects}a:active,a:hover{outline-width:0}abbr[title]{border-bottom:none;text-decoration:underline;text-decoration:underline dotted}b,strong{font-weight:bolder}dfn{font-style:italic}h1{font-size:2em;margin:.67em 0}mark{background-color:#ff0;color:#000}small{font-size:80%}sub,sup{font-size:75%;line-height:0;position:relative}sub{bottom:-.25em}sup{top:-.5em}img{border-style:none}svg:not(:root){overflow:hidden}code,kbd,pre,samp{font-family:monospace,monospace;font-size:1em}figure{margin:1em 40px}hr{box-sizing:content-box;height:0}button,input,select,textarea{font:inherit;margin:0}optgroup{font-weight:700}button,select{text-transform:none}[type=reset],[type=submit],button,html [type=button]{-webkit-appearance:button}[type=button]::-moz-focus-inner,[type=reset]::-moz-focus-inner,[type=submit]::-moz-focus-inner,button::-moz-focus-inner{border-style:none;padding:0}[type=button]:-moz-focusring,[type=reset]:-moz-focusring,[type=submit]:-moz-focusring,button:-moz-focusring{outline:ButtonText dotted 1px}fieldset{border:1px solid silver;margin:0 2px;padding:.35em .625em .75em}legend{color:inherit;display:table;max-width:100%;white-space:normal}textarea{overflow:auto}[type=number]::-webkit-inner-spin-button,[type=number]::-webkit-outer-spin-button{height:auto}[type=search]{-webkit-appearance:textfield;outline-offset:-2px}[type=search]::-webkit-search-cancel-button,[type=search]::-webkit-search-decoration{-webkit-appearance:none}::-webkit-input-placeholder{color:inherit;opacity:.54}::-webkit-file-upload-button{-webkit-appearance:button;font:inherit}</style>

		<!-- page styles -->
		<style>
			* {
				box-sizing: border-box;
				font-family: 'Open Sans', sans-serif;
				font-weight: lighter;
			}

			a {
				color: #4797CE;
				text-decoration: none;
				transition: color .3s ease;
			}
			a:hover {
				color: #2B71A1
			}

			.cf:before,
			.cf:after {
				content: " ";
				display: table;
			}
			.cf:after { clear: both; }

			.panel {
				background: rgba(255,255,255,0.9);
				padding: 20px;
			}
			@media screen and (min-width:768px){
				.panel { padding: 40px; }
			}


			/* HERO BLOCK */
			.hero {
				background: url('https://casestation.com/img/page-bg-min.jpg') center no-repeat;
				background-size: cover;
				min-height: 100vh;
			}
			.hero h2 {margin-top: 0;}
			.hero-aligner {
				height: 100vh;
				width: 100vw;
			}
			.hero-aligner td {
				padding:20px;
			}
			.hero-content .panel {
				margin: 20px auto 0;
				max-width: 45em;
			}
			.hero-content img {
				display: block;
				margin: 0 auto;
			}
			.hero-content ul {
				list-style: none;
				margin: 0;
				padding: 0;
				text-align: left;
			}
			.hero-content li {
				margin: 0;
				padding: .25em 0;
			}
			.regions { width: 100%; }
			@media screen and (min-width:768px){
				.hero-content {
					text-align: center;
				}
				.regions > li{
					float: left;
					padding-right: 10px;
					width: 25%;
				}
				.regions > li:last-child {
					padding-right:0;
				}
			}
		</style>

	</head>
	<body>
		<div class="hero">
			<table class="hero-aligner" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td>
						<div class="hero-content">
							<img src="https://casestation.com/img/logo.png" alt="casetstation logo">
							<div class="panel cf">
								<h2><?php _e('Page not found', 'zeus_cs'); ?></h2>
								<p>
									<?php _e('You seem to have stumbled off course.', 'zeus_cs'); ?><br>
									<a href="<?php echo site_url(); ?>"><?php _e('Go to the homepage.', 'zeus_cs'); ?></a>
								</p>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</body>
</html>
