<form role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label for="s" class="screen-reader-text"><?php _e('Search Case Station…', 'zeus_cs'); ?></label>
	<input type="text" placeholder="Search Case Station…" value="<?php echo get_search_query(); ?>" name="s" id="s" />
	<input type="submit" id="searchsubmit" class="screen-reader-text" value="<?php echo esc_attr_x( 'Search Case Station…', 'submit button' ); ?>" />
</form>
