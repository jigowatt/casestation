<?php

$theme_version = '1.7.2';



// Force development mode (useful for forcing cache refresh)
// @TODO turn off before production deployment
define( 'IS_DEV', false );

// detect staging site based on WPEngine URL
if( strpos($_SERVER['SERVER_NAME'], '.staging.') || strpos($_SERVER['SERVER_NAME'], '.wpengine.') ) {
  define( 'IS_STAGING', true);
} else {
  define( 'IS_STAGING', false);
}

// THEME VERSION (also used for cache busting)
if (IS_DEV || IS_STAGING) {
  define( 'VERSION', $theme_version.'--dev-'.date('U') );
} else {
  define( 'VERSION', $theme_version ); // @NOTE: should match the style.css version #
}

// store the theme URL
$template_url = get_template_directory_uri();
$template_dir = get_template_directory();


/**
 * Some helpful constants
 */
define( 'PATH_THEME', $template_url . '/' );            //theme root URL
define( 'PATH_TEMPLATES', '_templates/' );              //theme templates
define( 'PATH_PARTIALS', '_templates/partials/' );      //theme template partials
define( 'PATH_IMG', $template_url . '/dist/img/' );     //theme images dir
define( 'PATH_JS', $template_url . '/dist/js/' );       //theme javascript dir
define( 'PATH_PHP', $template_dir . '/_functional/' );  //theme php dir
define( 'ACF_ENABLED', class_exists('acf') );           //check if advanced custom fields is installed


/**
 * Debug logging and other useful dev tools
 */
include_once( PATH_PHP . 'dev-utils.php' );             //useful developer functions
if(function_exists('cs_log')) { cs_log('================== FUNCTIONS.PHP START ==================', false); }


/**
 * Includes
 */
include_once( PATH_PHP . 'wordpress-overrides.php' );   //overrides for core wordpress stuff
include_once( PATH_PHP . 'woocommerce/woocommerce.php' ); //overrides for WooCommerce plugin
include_once( PATH_PHP . 'navigation.php' );            //registers navigation for the theme
include_once( PATH_PHP . 'shortcodes.php' );            //registers shortcodes
include_once( PATH_PHP . 'widgets.php' );               //registers widget areas and widgets
include_once( PATH_PHP . 'theme-setup.php' );           //stuff to setup on every project
include_once( PATH_PHP . 'repeatable-content-blocks.php');

/**
 * Don't add .contain wrapper to page (conditionals)
 */
if (!function_exists('cs_dont_contain_page')) {
  function cs_contain_page()
  {
    global $post;

    if(function_exists('cs_log')) { cs_log('cs_contain_page()'); }

    if(!empty($post)){

      // get product category layouts
      $product_category_layout = cs_get_product_category_layout($post->ID);

      // get product type if we're on a product page
      $product = (is_product()) ? wc_get_product($post->ID) : null;
      $product_type = (!empty($product)) ? $product->get_type() : '' ;

      if(
        $product_type == 'ec3d' ||
        (in_array($product_category_layout, array('accessories', 'brand', 'collections', 'device')) && is_product_category())
      ){
        return false;
      } else {
        return true;
      }
    } else {
      return true;
    }
  }
}


/**
 * Hex to RGB colour converter
 */
if(!function_exists('hex2rgba')){
  function hex2rgba($hex, $alpha) {

    if(function_exists('cs_log')) { cs_log('hex2rgba()'); }

    $hex = str_replace("#", "", $hex);

    if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
    } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
    }
    $rgb = array($r, $g, $b);

    return 'rgba('.$rgb[0].', '.$rgb[1].', '.$rgb[2].', '.$alpha.')';
  }
}


/**
 * Get product category layout custom field value
 */
if (!function_exists('cs_get_product_category_layout')) {
  function cs_get_product_category_layout($product_id = null, $term_id = null)
  {
    global $wp_query, $product;

    if(function_exists('cs_log')) { cs_log('cs_get_product_category_layout()'); }

    if(empty($product_id) && empty($term_id) && empty($wp_query) && empty($product)){
      return false;
    }

    // make sure Advanced Custom Fields is active
    if(ACF_ENABLED && !is_shop()){

      // SPECIFIC POST (PRODUCT)
      if(!empty($product_id)){
        $terms = get_the_terms($product_id, 'product_cat');
        $layout = (!empty($terms)) ? get_field('layout', 'product_cat_'.$terms[0]->term_id) : null ;
        if (!empty($layout)) { return $layout; }
      }

      // SPECIFIC CATEGORY
      if(!empty($term_id)){
        $layout = get_field('layout', 'product_cat_'.$term_id);
        if(!empty($layout)) { return $layout; }
      }

      // IF NO POST/TERM IS PASSED, TRY TO WORK IT OUT
      if (empty($product_id) && empty($term_id)) {

        if (is_product()) {
          $terms = get_the_terms($product->get_id(), 'product_cat');
          $layout = (!empty($terms)) ? cs_get_product_category_layout($terms[0]->term_id) : null ;
          if(!empty($layout)) { return $layout; }
        }

        if (is_product_category()) {
          $cat = $wp_query->get_queried_object();
          $layout = (!empty($cat)) ? cs_get_product_category_layout(null, $cat->term_id) : null ;
          if(!empty($layout)) { return $layout; }
        }

      }

    }

    if (is_shop() && !empty($term_id)) {
      $layout = get_field('layout', 'product_cat_'.$term_id);
      if(!empty($layout)) { return $layout; }
    }

    // default return value
    return false;
  }
}


/**
 * Social Sharing Links
 */
if (!function_exists('cs_social_sharing_links')) {
  function cs_social_sharing_links($icon_colour = '', $share_text = false)
  {
    global $wp;

    if(function_exists('cs_log')) { cs_log('cs_social_sharing_links()'); }

    if(!empty($icon_colour)){
      $icon_colour = '-'.$icon_colour;
    }

    $url = home_url(add_query_arg(array(),$wp->request));

    echo     '<p class="social-sharing-links">';
    if ($share_text){
      echo     '<span class="share">'.__('Share', 'zeus_cs').'</span>';
    }
    echo       '<a class="nofancybox nolightbox" target="_blank" href="https://twitter.com/intent/tweet?url='.$url.'"><img src="'.PATH_IMG.'icon-twitter'.$icon_colour.'.svg" title="'.__('Share on twitter', 'zeus_cs').'" width="36" height="36" ></a>';
    echo       '<a class="nofancybox nolightbox" target="_blank" href="https://www.facebook.com/sharer.php?u='.$url.'"><img src="'.PATH_IMG.'icon-facebook'.$icon_colour.'.svg" title="'.__('Share on facebook', 'zeus_cs').'" width="36" height="36" ></a>';
    echo       '<a class="nofancybox nolightbox" target="_blank" href="https://www.pinterest.com/pin/create/button/?url='.$url.'"><img src="'.PATH_IMG.'icon-pinterest'.$icon_colour.'.svg" title="'.__('Pin it!', 'zeus_cs').'" width="36" height="36" ></a>';
    echo     '</p>';
  }
}

/**
 * Add analytics/tracking to pages
 */
if (!function_exists('cs_javascript_tracking_head')) {
  add_action('wp_head', 'cs_javascript_tracking_head');
  function cs_javascript_tracking_head()
  {
    if(function_exists('cs_log')) { cs_log('cs_javascript_tracking_head()'); }

    if(ACF_ENABLED && !IS_STAGING){
      if(!is_user_logged_in() || !current_user_can('edit_posts')){
        $tracking = get_field('js_head', 'option');
        echo $tracking;
      }
    }
  }
}
if (!function_exists('cs_javascript_tracking_body_top')) {
  add_action('cs_body_top', 'cs_javascript_tracking_body_top');
  function cs_javascript_tracking_body_top()
  {
    if(function_exists('cs_log')) { cs_log('cs_javascript_tracking_body_top()'); }

    if(ACF_ENABLED && !IS_STAGING){
      if(!is_user_logged_in() || !current_user_can('edit_posts')){
        $tracking = get_field('js_body_top', 'option');
        echo $tracking;
      }
    }
  }
}
if (!function_exists('cs_javascript_tracking_body_bottom')) {
  add_action('wp_footer', 'cs_javascript_tracking_body_bottom');
  function cs_javascript_tracking_body_bottom()
  {
    if(function_exists('cs_log')) { cs_log('cs_javascript_tracking_body_bottom()'); }

    if(ACF_ENABLED && !IS_STAGING){
      if(!is_user_logged_in() || !current_user_can('edit_posts')){
        $tracking = get_field('js_body_bottom', 'option');
        echo $tracking;
      }
    }
  }
}


//==============================
//  WP ALL IMPORT FUNCTIONS

/**
 *  These 7 functions take the 'Category' field from 'WP All Import' and return broken out values
 */
function casestation_wp_all_import_get_categories( $data )
{
    $exploded = array_map( "trim", explode( ",", $data ) );
    return implode( ",", array( 'Collaborations', $exploded[1] ) );
}
function casestation_wp_all_import_get_brand( $data )
{
    $exploded = array_map( "trim", explode( ",", $data ) );
    return $exploded[0];
}
function casestation_wp_all_import_get_artist( $data )
{
    $exploded = array_map( "trim", explode( ",", $data ) );
    return $exploded[1];
}
function casestation_wp_all_import_get_artwork( $data )
{
    $exploded = array_map( "trim", explode( ",", $data ) );
    return $exploded[2];
}
function casestation_wp_all_import_get_device( $data )
{
    $exploded = array_map( "trim", explode( ",", $data ) );
    return $exploded[3];
}
function casestation_wp_all_import_get_style( $data )
{
    $exploded = array_map( "trim", explode( ",", $data ) );
    return $exploded[4];
}
function casestation_wp_all_import_get_finish( $data )
{
    $exploded = array_map( "trim", explode( ",", $data ) );
    return $exploded[5];
}


/**
 * Sets currency position based on woocommerce option
 */
if (!function_exists('cs_format_currency_position')) {
  function cs_format_currency_position($amount){
    $position = get_option( 'woocommerce_currency_pos' );
    $symbol = (function_exists('get_woocommerce_currency_symbol')) ? get_woocommerce_currency_symbol() : '£';

    switch ( $position ) {
      case 'left' :
        return $symbol . $amount;
      break;
      case 'right' :
        return $amount . $symbol;
      break;
      case 'left_space' :
        return $symbol . ' ' . $amount;
      break;
      case 'right_space' :
        return $amount . ' ' . $symbol;
      break;
    }
  }
}
