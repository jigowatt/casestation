<?php
/**
 * Template name: Legal
 */


if(function_exists('cs_log')) {
	cs_log('LOADED _templates/tpl-legal.php');
}
?>
<?php get_header(); ?>
<div class="legal-content">
	<?php get_template_part(PATH_PARTIALS.'loop-basic'); ?>
</div>
<?php get_footer(); ?>
