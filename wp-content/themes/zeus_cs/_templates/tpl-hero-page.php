<?php
/**
 * Template Name: Hero Page
 */


if(function_exists('cs_log')) {
 cs_log('LOADED _templates/tpl-hero-page.php');
}

get_header(); ?>

<?php

	get_template_part(PATH_PARTIALS.'loop-hero-page');

	cs_content_blocks();

	// If we're on a single post or page
	if(is_single()){

		// output next/prev paging
		get_template_part(PATH_PARTIALS.'block-single_paging');

		// and comments are enabled
		if ( comments_open() || get_comments_number() ) :

			// output comments and comments form
			comments_template();
		endif;

		// output next/prev paging
		get_template_part(PATH_PARTIALS.'block-single_paging');
	}

	// If we're on a "listing" page (archive)
	else {

		// get numbered paging
		get_template_part(PATH_PARTIALS.'block-archive_paging');

	}



?>

<?php get_footer(); ?>
