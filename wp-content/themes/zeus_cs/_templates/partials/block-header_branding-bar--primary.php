<header class="branding-bar branding-bar--primary">
  <div class="contain">
    <div class="grid flex">
      <div class="grid__item one-half desk-one-sixth">
        <a href="<?php echo site_url() ?>" class="logo">
          <?php
            if(ACF_ENABLED){
              $primary_logo = get_field('primary_logo', 'option');
              echo wp_get_attachment_image($primary_logo['ID'], array(150, 37));
            }
          ?>
        </a>
      </div>
      <div class="grid__item desk-four-sixths menu menu--main-primary">
        <?php
          $defaults = array(
            'theme_location'  => 'main_menu',
            'menu'            => '',
            'container'       => 'nav',
            'container_class' => '',
            'container_id'    => '',
            'menu_class'      => '',
            'menu_id'         => '',
            'echo'            => true,
            'fallback_cb'     => '',
            'before'          => '',
            'after'           => '',
            'link_before'     => '',
            'link_after'      => '',
            'items_wrap'      => '<ul id="%1$s" class="%2$s flex">%3$s</ul>',
            'depth'           => 3,
            'walker'          => ''
          );
          wp_nav_menu( $defaults );
        ?>
        <div id="branding-bar__search" class="branding-bar__search"><?php get_search_form(); ?></div>
      </div>
      <div class="grid__item one-half desk-one-sixth">
        <div class="utility flex">
          <?php echo get_template_part(PATH_PARTIALS.'block', 'header_lang-minicart') ?>

          <div class="burger">
            <div class="burger-brick"></div>
            <div class="burger-brick middle"></div>
            <div class="burger-brick"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
