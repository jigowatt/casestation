<div class="footer-columns">
  <div class="contain">

    <h2><?php _e('Useful Information', 'zeus_cs') ?></h2>

    <div class="grid flex">
      <div class="grid__item one-whole lap-and-up-one-third">
        <?php dynamic_sidebar('footer-links'); ?>
        <ul class="app-badges">
        <?php
          if (ACF_ENABLED) {
            if( have_rows('app_badges', 'option') ): while ( have_rows('app_badges', 'option') ) : the_row();
              $link = get_sub_field('link');
              $image = get_sub_field('image');
              echo '<li><a href="'.$link.'" target="new"><img src="'.$image['url'].'" alt="'.$image['alt'].'" /></a></li>';
            endwhile; endif;
          }
        ?>
        </ul>
      </div>
      <div class="grid__item one-whole lap-and-up-one-third">
        <h3><?php _e('Keep in touch', 'zeus_cs'); ?></h3>
        <ul class="social-networks">
          <?php
            if (ACF_ENABLED) {
              if( have_rows('social_networks', 'option') ): while ( have_rows('social_networks', 'option') ) : the_row();
                $name = get_sub_field('name');
                $icon = get_sub_field('icon');
                $link = get_sub_field('link');
                echo '<li><a href="'.$link.'" target="new"><img src="'.$icon['url'].'" alt="'.$icon['alt'].'" title="'.$name.'" /></a></li>';
              endwhile; endif;
            }
          ?>
        </ul>
        <div class="payment-menu-currency">
          <h3><?php _e('Your Payment is Secure', 'zeus_cs'); ?></h3>
          <ul class="payment-icons">
          <?php
            if (ACF_ENABLED) {
              $icons = get_field('payment_icons', 'option');
              foreach ($icons as $icon => $value) {
                echo '<li><img src="'.PATH_IMG.'payment/'.$value.'.svg" alt="'.$icon.'" /></li>';
              }
            }
          ?>
          </ul>
          <div class="copyright"><?php
              if (ACF_ENABLED) {
                the_field('footer_links_and_copyright', 'option');
              }
            ?>
          </div>
        </div>

      </div>
      <div class="grid__item one-whole lap-and-up-one-third">
        <?php dynamic_sidebar('footer-text'); ?>
      </div>
    </div>
  </div>
</div>
