<header class="branding-bar branding-bar--secondary">
  <div class="contain">
    <div class="grid flex">
      <div class="grid__item one-half desk-one-sixth">
        <a href="<?php echo site_url() ?>" class="logo">
          <?php
            if (ACF_ENABLED) {
              $secondary_logo = get_field('secondary_logo', 'option');
              echo wp_get_attachment_image($secondary_logo['ID'], array(150, 35));
            }
          ?>
        </a>
      </div>
      <?php
        $defaults = array(
          'theme_location'  => 'main_menu_secondary',
          'menu'            => '',
          'container'       => 'nav',
          'container_class' => 'grid__item desk-four-sixths menu menu--main-secondary',
          'container_id'    => '',
          'menu_class'      => '',
          'menu_id'         => '',
          'echo'            => true,
          'fallback_cb'     => '',
          'before'          => '',
          'after'           => '',
          'link_before'     => '',
          'link_after'      => '',
          'items_wrap'      => '<ul id="%1$s" class="%2$s flex">%3$s</ul>',
          'depth'           => 3,
          'walker'          => ''
        );
        wp_nav_menu( $defaults );
      ?>
      <div class="grid__item one-half desk-one-sixth">
        <div class="utility flex">
          <?php echo get_template_part(PATH_PARTIALS.'block', 'header_lang-minicart') ?>

          <div class="burger">
            <div class="burger-brick"></div>
            <div class="burger-brick middle"></div>
            <div class="burger-brick"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
