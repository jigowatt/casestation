<?php
// output site-wide alert(s)
if (ACF_ENABLED) {
  if( have_rows('site_notices', 'option') ):
    echo '<div class="cs-notices">';
    while ( have_rows('site_notices', 'option') ) : the_row();
      $notice_text = get_sub_field('text');

      if(!empty($notice_text)){
        $notice_id = sanitize_title($notice_text);
          
        // if the notice has been dismissed, don't show it
        if(empty($_COOKIE[$notice_id])){

          // visual options
          $notice_background = get_sub_field('notice_background');
          $text_color        = get_sub_field('text_color');

          // schedule dates
          $today       = strtotime(current_time('Ymd'));
          $start_date  = strtotime(get_sub_field('start_date', false, false));
          $has_started = ($today >= $start_date) ? true : false;
          $end_date    = strtotime(get_sub_field('end_date', false, false));
          $has_ended   = (!empty($end_date) && $today > $end_date) ? true : false;

          if (
            ($has_started && (!empty($end_date) && !$has_ended)) || // has started and hasn't yet ended, or…
            ($has_started && empty($end_date))                      // has started and no end date set
          ) {
            echo '<div id="'.$notice_id.'" class="cs-notice" style="background-color: '.$notice_background.'; color: '.$text_color.' !important;"><div class="contain">'.$notice_text.'</div></div>';
          }
        }
      }
    endwhile;
    echo '</div>';
  endif;
}
