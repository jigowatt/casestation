<?php
  if (ACF_ENABLED) {
    if( have_rows('company_logos', 'option') ):
    ?>
      <div class="people-are-talking">
        <ul class="logos flex">
        <?php
          while ( have_rows('company_logos', 'option') ) : the_row();
            $logo = get_sub_field('logo');
            $link = get_sub_field('link');
            echo '<li>';
              if(!empty($link)){
                echo '<a href="'.$link.'">';
              }
                echo '<img src="'.$logo['url'].'" alt="'.$logo['alt'].'" />';
              if(!empty($link)){
                echo '</a>';
              }
            echo '</li>';
          endwhile;
          ?>
        </ul>
      </div>
    <?php
    endif;
  }