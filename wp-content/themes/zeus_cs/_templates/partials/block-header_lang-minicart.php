<a href="#branding-bar__search" class="icon icon--search">Search</a>

<?php
  // If WooCommerce is installed, add the minicart
  // This will also be updated via AJAX when a product is added,
  // see functional/woocommerce-overrides.php
  if(class_exists('WooCommerce')) {
?>
    <div class="woocommerce">
      <div class="cs-mini-cart">
        <a class="cart-icon" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>">
          (<?php echo sprintf ( _n( '%d', '%d', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() ); ?>)
        </a>
        <div class="cs-mini-cart__details">
          <?php woocommerce_mini_cart(); ?>
        </div>
      </div>
    </div>
<?php
  }
?>
