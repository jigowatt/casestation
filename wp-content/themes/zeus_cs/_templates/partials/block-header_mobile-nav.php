<nav class="mobile-nav-container">
  <div class="scroller">
    <?php dynamic_sidebar( 'mobile-menu-area' ) ?>
  </div>

  <div class="mobile-utility">
    <?php
    if(ACF_ENABLED):
      $locale = get_field('current_locale', 'option');
    ?>
      <nav class="lang-selector" style="margin-bottom: 10px;">
        <a class="current-lang"><img class="flag" src="<?php echo PATH_IMG.'flags/'.$locale.'.svg'; ?>"> <?php _e('Change region', 'zeus_cs'); ?></a>
      </nav>
    <?php
    endif;
    
    get_search_form(); ?>
  </div>
</nav>
