<div class="lang-chooser-container">
  <div class="lang-chooser">
    <a class="close">&times;</a>
  	<h2><?php _e('Choose Your Region', 'zeus_cs'); ?></h2>
  	<p>
      <?php _e('We recommend you visit your local site for the most relevant content and information.', 'zeus_cs'); ?>
  	</p>
  	<ul class="regions">
  		<li>
  			<h3>Americas</h3>
  			<ul class="countries">
  				<li><a href="https://us.casestation.com/">United States</a></li>
  				<li><a href="https://ar.casestation.com/">Argentina</a></li>
  			</ul>
  		</li>
  		<li>
  			<h3>Europe</h3>
  			<ul class="countries">
  				<li><a href="https://uk.casestation.com/">United Kingdom</a></li>
  				<li><a href="https://de.casestation.com/">Germany</a></li>
  				<li><a href="https://fr.casestation.com/">France</a></li>
  			</ul>
  		</li>
  		<li>
  			<h3>Middle East</h3>
  			<ul class="countries">
  				<li><a href="https://casestation.com/il/">Israel</a></li>
  			</ul>
  		</li>
  		<li>
  			<h3>Asia Pacific</h3>
  			<ul class="countries">
  				<li><a href="https://au.casestation.com/">Australia</a></li>
  				<li><a href="https://in.casestation.com/">India (English)</a></li>
  				<li><a href="https://www.casestation.com.cn/en">China (English)</a></li>
  				<li><a href="https://www.casestation.com.cn/">China (Chinese)</a></li>
  				<li><a href="https://jp.casestation.com/">Japan</a></li>
  				<li><a href="https://kr.casestation.com/">South Korea</a></li>
  			</ul>
  		</li>
  	</ul>
  </div>
</div>
