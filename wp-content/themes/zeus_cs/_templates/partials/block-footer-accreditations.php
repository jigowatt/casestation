<div class="accreditations">
  <div class="contain">
    <div class="badges">
      <?php
        if (ACF_ENABLED) {
          if( have_rows('accreditations', 'option') ): while ( have_rows('accreditations', 'option') ) : the_row();
            $badge = get_sub_field('badge');
            echo '<img src="'.$badge['url'].'" alt="'.$badge['alt'].'" />';
          endwhile; endif;
        }
      ?>
    </div>
  </div>
</div>
