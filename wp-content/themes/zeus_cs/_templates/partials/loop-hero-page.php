<?php

	if ( have_posts() ) :

		while ( have_posts() ) : the_post(); ?>

		<article <?php post_class(); ?>>

			<div class="post__content"><?php

				if(is_singular()):
					the_post_thumbnail('large');
					the_content();	// full article
				else:
					the_post_thumbnail('thumbnail');
					the_excerpt();	// article listing
				endif;?>

			</div>

		</article><?php

	endwhile; endif;

?>
