<?php
if(function_exists('cs_log')) {
 cs_log('LOADED footer.php');
}
?>
			<?php if (cs_contain_page()): ?>
			</div> <!-- .contain -->
			<?php endif; ?>
		</div><!-- .content-container -->

		<footer class="main-footer">

			<?php get_template_part(PATH_PARTIALS.'block', 'footer-people-are-talking'); ?>
			<?php get_template_part(PATH_PARTIALS.'block', 'footer-newsletter'); ?>
			<?php get_template_part(PATH_PARTIALS.'block', 'footer-columns'); ?>
			<?php get_template_part(PATH_PARTIALS.'block', 'footer-accreditations'); ?>

		</footer>
		
	</div> <!-- /page wrapper -->

	<?php get_template_part(PATH_PARTIALS.'block', 'language-chooser'); ?>

	<?php wp_footer() ?>
</body>
</html>
