<?php get_header(); ?>

<?php

    $searchterm = esc_html(get_query_var('s'));
    if (!empty($searchterm)): ?>
        <h1 style="text-align: left;"><?php echo __('Search results: ', 'jigowatt') .'&ldquo;'.$searchterm.'&rdquo;'; ?></h1><?php
    else : ?>
        <h1 style="text-align: left;"><?php echo __('Search results…', 'jigowatt'); ?></h1><?php
    endif;

?>
    <p class="alert"><?php
        _e('If your search results show the wrong device model, try putting quote marks around what you searched!', 'zeus_cs'); ?>
    </p>
<?php

	get_template_part(PATH_PARTIALS.'loop-basic');

    // get numbered paging
    get_template_part(PATH_PARTIALS.'block-archive_paging');

?>

<?php get_footer(); ?>