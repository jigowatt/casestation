<?php

if(function_exists('cs_log')) {
  cs_log('LOADED _functional/theme-setup.php');
}

/**
 * Add localisation
 */
load_theme_textdomain( 'zeus_cs', $template_dir.'/languages' );


/**
 * Add Image Sizes
 */
if (!function_exists('cs_theme_image_sizes')) {
	add_action( 'after_setup_theme', 'cs_theme_image_sizes' );
	function cs_theme_image_sizes()
	{
		// cropped thumbnails
		add_image_size( 'half-thumb', 620, 620, true );
		add_image_size( 'third-thumb', 410, 410, true );
		add_image_size( 'quarter-thumb', 320, 320, true );
		
		// full height images
		add_image_size( 'half', 620, 620, false );
		add_image_size( 'third', 410, 410, false );
		add_image_size( 'quarter', 320, 320, false );

	}
}


/**
 * Add Theme Support
 */
if (!function_exists('cs_theme_supports')) {
	add_action( 'after_setup_theme', 'cs_theme_supports' );
	function cs_theme_supports()
	{
		add_theme_support( 'post-thumbnails' ); //thumbnails (featured image)
		add_theme_support( 'menus' );           //custom menus
		add_theme_support( 'html5' );           //html5
	}
}


/**
 * Dashboard theme version number display
 */
if (!function_exists('cs_output_theme_version_in_dashboard')){
	add_filter( 'update_right_now_text', 'cs_output_theme_version_in_dashboard' );
	function cs_output_theme_version_in_dashboard( $content ){
		$content = str_replace('%2$s', '%2$s '.VERSION, $content);
		return $content;
	}
}


/**
 * Load Scripts and Styles
 *
 * Note: IE specific imports can found further down this file
 */
if (!function_exists('cs_enqueue_all_the_things')) {
	add_action( 'wp_enqueue_scripts', 'cs_enqueue_all_the_things' );
	function cs_enqueue_all_the_things()
	{

		// Scripts
		wp_enqueue_script( 'inview', 'https://cdnjs.cloudflare.com/ajax/libs/protonet-jquery.inview/1.1.2/jquery.inview.min.js', array('jquery'));
		wp_enqueue_script( 'select2', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js', array('jquery'));
		wp_enqueue_script( 'theme', PATH_JS.'theme.js', array( 'select2', 'jquery', 'inview' ), VERSION );

		// enable some PHP values to be available to our JavaScript
		$phpVars = array(
			'js' => PATH_JS,
			'img' => PATH_IMG,
			'uploads' => site_url()."/wp-content/uploads/",
			'comingsoon_txt' => __( 'Coming soon', 'zeus_cs' ),
			'preorder_txt' => __( 'Pre-order - Ships ', 'zeus_cs' ),
			'default_designer_txt' => __( 'Create now', 'zeus_cs' ),
			'default_txt' => __( 'Add to basket', 'zeus_cs' ),
		);
		wp_localize_script( 'theme', 'phpVars', $phpVars );

		// Styles
		wp_enqueue_style( 'select2', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css', false, null );
		wp_enqueue_style( 'wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Nunito+Sans:300,300i,600,600i&Montserrat', false, VERSION );
		wp_register_style( 'main', PATH_THEME.'style.css', null, VERSION );
		wp_style_add_data( 'main', 'style-rtl', 'replace' );
		wp_enqueue_style( 'main' );

	}
}

/**
 * Add page/post slug to <body> "class" attribute
 */
if (!function_exists('cs_add_body_class')) {
	add_filter( 'body_class', 'cs_add_body_class' );
	function cs_add_body_class( $classes )
	{
		global $post;

		if ( isset( $post ) ) {
			$classes[] = $post->post_type . ' ' . $post->post_name;
		}

		$blogname = get_bloginfo( 'name' );

		$classes[] = sanitize_title( $blogname );

		return $classes;
	}
}

/*  IE js header
/* ------------------------------------ */
if (!function_exists('cs_ie_js_header')) {
	function cs_ie_js_header () {
		echo '<!--[if lt IE 9]>'. "\n";
		echo '<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>'. "\n";
		echo '<script src="' . PATH_JS . 'lib/selectivizr.min.js"></script>'. "\n";
		echo '<![endif]-->'. "\n";
	}
	add_action( 'wp_head', 'cs_ie_js_header' );
}

/*  IE js footer
/* ------------------------------------ */
if (!function_exists('cs_ie_js_footer')) {
	function cs_ie_js_footer () {
		echo '<!--[if lt IE 9]>'. "\n";
		echo '<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>'. "\n";
		echo '<![endif]-->'. "\n";
	}
	add_action( 'wp_footer', 'cs_ie_js_footer', 20 );
}

/**
 * Add Advanced Custom Fields Options page when plugin is active
 */
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}


/**
 * Prevent DDoS attacks using pingbacks
 */
if (!function_exists('remove_x_pingback')) {
	function remove_x_pingback($headers) {
		unset($headers['X-Pingback']);
		return $headers;
	}
	add_filter('wp_headers', 'remove_x_pingback');
}


/**
 * Provide a button for bug reporting
 */
add_action('admin_bar_menu', 'cs_report_bug_button', 19);
function cs_report_bug_button($wp_admin_bar){
  $args = array(
      'id' => 'cs_bug_report',
      'title' => 'Report Bug',
      'href' => 'https://bitbucket.org/jigowatt/casestation/issues/new',
      'meta' => array(
        'class' => 'bug-button',
        'target' => 'bugreport',
      )
    );
  $wp_admin_bar->add_node($args);
  echo '<style>#wpadminbar .bug-button a:before { content: "\f468"; }</style>';
}


/**
 * Remove wordpress logo menubar node
 */
add_action( 'admin_bar_menu', 'remove_wp_logo', 999 );
function remove_wp_logo( $wp_admin_bar ) {
  $wp_admin_bar->remove_node( 'wp-logo' );
}
