<?php

/**
 * Jigowatt logging
 */
if ( ! function_exists( 'cs_log' ) )
{
	/**
	 * Logs to the debug log when you enable wordpress debug mode.
	 *
	 * @param string $from_file is the name of the php file that you are logging from.
	 * defaults to JIGOWATT if non is supplied.
	 * @param mixed $message this can be a regular string, array or object
	 */
	function cs_log( $message, $indent = true, $from_file = 'ZEUS_CS_v'.VERSION )
	{
		if ( WP_DEBUG === true ) {
			$indent = ($indent) ? '    ' : '' ;
			if ( is_array( $message ) || is_object( $message )) {
				error_log( $from_file.': '.$indent.print_r($message, true) );
			} else {
				error_log( $from_file.': '.$indent.$message );
			}
		}
	}
}

?>
