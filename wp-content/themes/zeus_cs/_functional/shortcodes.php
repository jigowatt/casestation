<?php

if(function_exists('cs_log')) {
  cs_log('LOADED _functional/shortcodes.php');
}

/**
 * Creates a new grid container
 * @param  string $content content to put between the div tags
 * @return string          html for the grid container
 */
if (!function_exists('cs_grid')) {
	function cs_grid($atts, $content=null)
	{
		return '<div class="grid">'.do_shortcode(shortcode_unautop($content)).'</div>';
	}
	add_shortcode( 'grid', 'cs_grid' );
}


/**
 * Creates a new grid column
 * @param  array $atts    group of options
 * @param  string $content content to put between the div tags
 * @return string          html for the column
 */
if (!function_exists('cs_grid_item')) {
	function cs_grid_item($atts, $content=null)
	{
		extract( shortcode_atts( array(
			'palm' => '',
			'lap' => '',
			'lap_and_up' => '',
			'desk' => '',
			'desk_wide' => '',
			'default' => ''
		), $atts));

		$class = '';

		if(!empty($atts)){
			foreach ($atts as $name => $value) {

				if(!empty($value)){
					switch ($name){
						case 'palm':
							$class .= 'palm-'.$value.' ';
							break;
						case 'lap':
							$class .= 'lap-'.$value.' ';
							break;
						case 'lap_and_up':
							$class .= 'lap-and-up-'.$value.' ';
							break;
						case 'desk':
							$class .= 'desk-'.$value.' ';
							break;
						case 'desk_wide':
							$class .= 'desk-wide-'.$value.' ';
							break;
						case 'default':
							$class .= $value.' ';
							break;
					}
				}
			}
		}

		return '<div class="grid__item '.$class.'">'.do_shortcode(shortcode_unautop($content)).'</div>';
	}
	add_shortcode( 'grid_item', 'cs_grid_item' );
}
