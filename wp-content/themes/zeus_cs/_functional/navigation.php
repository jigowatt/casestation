<?php

if(function_exists('cs_log')) {
  cs_log('LOADED _functional/navigation.php');
}

/**
 * Register menus
 */
register_nav_menus( array(
	'main_menu' => 'Menu in the primary branding bar',
	'main_menu_secondary' => 'Menu in the secondary branding bar',
	'locale_menu' => 'Menu for links to different language sites',
	'footer_menu' => 'Footer links'
) );

?>
