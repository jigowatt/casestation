<?php

/**
 * Main repeatable content blocks function (loop)
 *
 * @param [string] $target tells ACF which post/tax-term id to grab data with
 */
function cs_content_blocks($target = ''){
    if( have_rows('content_blocks', $target) ):
        while ( have_rows('content_blocks', $target) ) : the_row();
            cs_output_content_block();
        endwhile;
    endif;    
}

/**
 * Outputs each repeatable content block's containers, 
 * then calls individual block output functions
 * @param  string $layout layout name for the block being looped
 */
function cs_output_content_block()
{
    global $block_id;
    $block_id++;

    $layout = get_row_layout();
    
    // detect full-width modules and provide switch to each module
    $close_container = ( 
        !empty(get_sub_field('background_color')) || 
        !empty(get_sub_field('background_image')) || 
        get_sub_field('full_width') ||
        $layout == 'two_graphics'
    ) ? true : false;

    // open module markup
    $wrapper_start  = ($close_container) ? '<!-- open full-width --><div class="full-width">' : '' ;
    $wrapper_start .= '<!-- open content-block '.$block_id.' --><div class="'.$layout.' content-block-'.$block_id.' cf">';

    // close module markup
    $wrapper_end    = '</div><!-- close content-block '.$block_id.' -->';
    $wrapper_end   .= ($close_container) ? '</div><!-- close full-width -->' : '' ;
    
    // output block content
    echo $wrapper_start;
    switch ($layout) {
        case 'image_links':
            cs_output_content_block__image_links($close_container);
            break;
        case 'collabs_grid':
            cs_output_content_block__collabs_grid($close_container);
            break;
        case 'full_width_graphic_text_overlay':
            cs_output_content_block__full_width_graphic_text_overlay($close_container);
            break;
        case 'slider':
            cs_output_content_block__slider($close_container);
            break;
        case 'full_width_text_editor':
            cs_output_content_block__full_width_text_editor($close_container);
            break;
        case 'two_graphics':
            cs_output_content_block__two_graphics($close_container);
            break;
        case 'instagram_feed':
            cs_output_content_block__instagram_feed($close_container);
            break;
        case 'horizontal_rule':
            cs_output_content_block__horizontal_rule($close_container);
            break;
        case 'features_list':
            cs_output_content_block__features_list($close_container);
            break;
        case 'testimonials_list':
            cs_output_content_block__testimonials_list($close_container);
            break;
        case 'video_row':
            cs_output_content_block__video_row($close_container);
            break;
    }    
    echo $wrapper_end;

}


/**
 * Output the image links content block
 */
function cs_output_content_block__image_links($close_container){

    $background_color = get_sub_field('background_color');
    $background_image = get_sub_field('background_image');
    $background_repeat = get_sub_field('background_repeat');
    $block_title = get_sub_field('block_title');
    $block_description = get_sub_field('block_description');
    $block_layout = get_sub_field('format');
    $block_items = get_sub_field('items');

    if($close_container){ 
        
        // define background div styles
        $style = '';
        $style .= (!empty($background_color)) ? 'background-color: '.$background_color.';' : '';
        $style .= (!empty($background_image)) ? 'background-image: url('.wp_get_attachment_image_src($background_image["id"], "large")[0].');' : '';
        $style .= (!empty($background_repeat)) ? 'background-repeat: repeat;' : 'background-size: cover;';
        
        echo '<div class="background-container" style="'.$style.'">'; // open background container
        echo '<div class="contain">'; // open container div for content
    }

    // output the block title and description
    if(!empty($block_title)) { echo '<h2>'.$block_title.'</h2>'; }
    if(!empty($block_description)) { echo '<div class="description">'.$block_description.'</div>'; }

    // loop through the items
    if(have_rows('items')):

        $item_count = 0;

        echo '<div class="grid image-links__items">';

        while(have_rows('items')): the_row('items'); $item_count++;

            // get unconditional fields
            $type = get_sub_field('type');
            $image = get_sub_field('image');
            $aspect_ratio = $image['sizes']['large-height']/$image['sizes']['large-width']*100;
            $title = get_sub_field('title');
            $price = get_sub_field('price');
            $link = get_sub_field('link');
            $overlay_content = get_sub_field('overlay_content');
            $overlay_background = get_sub_field('overlay_background');

            // decide grid class based on $layout
            $class = 'block-'.$item_count.' grid__item ';
            switch($block_layout){
                case 'half':
                    $class .= "desk-one-half desk-wide-one-half";
                    break;
                case 'third':
                    $class .= "desk-one-third desk-wide-one-third";
                    break;
                case 'quarter':
                    $class .= "desk-one-quarter desk-wide-one-quarter";
                    break;
                case 'half-third': // if it's the first, second, 5n+1 or 5n+2
                    if ( $item_count == 1 || $item_count == 2 || $item_count % 5 == 1 || $item_count % 5 == 2 )
                        $class .= "desk-one-half desk-wide-one-half";
                    else 
                        $class .= "desk-one-third desk-wide-one-third";
                    break;
                case 'third-half': // if it's the fourth, fifth, 4n+1 or 4n+2
                    if ( $item_count == 4 || $item_count == 5 || $item_count % 5 == 0 || $item_count % 5 == 4 )
                        $class .= "desk-one-half desk-wide-one-half";
                    else 
                        $class .= "desk-one-third desk-wide-one-third";
                    break;
            }

            // item
            echo '<div class="'.$class.' image-links__item invisible">';

                echo '<div class="img-links__item__imaage-container">';

                    // output image (with/without link)
                    if(!empty($overlay_background) && !empty($overlay_content) && !empty($link)){
                        echo'<div class="card-fade" style="padding-top: '.$aspect_ratio.'%;">';
                            echo '<a class="card" href="'.$link.'">';
                                echo '<div class="front">'.wp_get_attachment_image($image['ID'], 'medium-large').'</div>';
                                echo '<div class="back"><div style="background-color: '.$overlay_background.';">'.$overlay_content.'</div></div>';
                            echo '</a>';
                        echo '</div>';
                    }
                    else {
                        if(!empty($link)) { echo '<a class="image-links__item__image-link" href="'.$link.'">'; }
                            echo wp_get_attachment_image($image['ID'], 'medium-large');
                        if(!empty($link)) { echo '</a>'; }
                    }

                echo '</div>';
                    
                // output title and price
                if(!empty($title) || !empty($price)) { echo '<div class="image-links__item__meta">'; }
                if(!empty($title)) { echo '<span class="image-links__item__title">'.$title.'</span>'; }
                if(!empty($price)) { echo '<span class="image-links__item__price">'.cs_format_currency_position($price).'</span>'; }
                if(!empty($title) || !empty($price)) { echo '</div>'; }

                // list additional links
                if(have_rows('links')):
                    echo '<div class="image-links__item__secondary-links">';
                    while(have_rows('links')): the_row('link');
                        $type = get_sub_field('type');
                        $link = get_sub_field('link');

                        if(!empty($link)) { echo '<a href="'.$link['url'].'">'.$link['title'].'</a>'; }
                    endwhile;
                    echo '</div>';
                endif;

            echo '</div>'; // close item
        endwhile;

        echo '</div>'; // close items wrapper
    endif;

    if($close_container){ 
        echo '</div>'; // close container
        echo '</div>'; // close background container
    }
    
}


function cs_output_content_block__collabs_grid($close_container){

    $background_color = get_sub_field('background_color');
    $background_image = get_sub_field('background_image');
    $background_repeat = get_sub_field('background_repeat');
    $block_title = get_sub_field('block_title');
    $block_description = get_sub_field('block_description');
    $button = get_sub_field('button');
    $grid_info_position = get_sub_field('grid_info_position');
    $item_width = get_sub_field('item_width');

    if($close_container){ 
        
        // define background div styles
        $style = '';
        $style .= (!empty($background_color)) ? 'background-color: '.$background_color.';' : '';
        $style .= (!empty($background_image)) ? 'background-image: url('.wp_get_attachment_image_src($background_image["id"], "large")[0].');' : '';
        $style .= (!empty($background_repeat)) ? 'background-repeat: repeat;' : 'background-size: cover;';
        
        echo '<div class="background-container" style="'.$style.'">'; // open background container
        echo '<div class="contain">'; // open container div for content
    }

    // loop through the items
    if(have_rows('items')):

        $item_count = 0;

        echo '<div class="grid flex collab_grid__items">';

        while(have_rows('items')): the_row('items'); $item_count++;

            // get unconditional fields
            $type = get_sub_field('type');
            $image = get_sub_field('image');
            $link = get_sub_field('link');

            if($grid_info_position == $item_count){
                echo '<div class="block-'.$item_count.' grid__item portable-one-half desk-one-'.$item_width.' desk-wide-one-'.$item_width.' collab_grid__item invisible">';
                    echo '<div class="padder">';
                        echo '<div class="meta-block">';
                            if(!empty($block_title)){ echo '<h2>'.$block_title.'</h2>'; }
                            if(!empty($block_description)){ 
                                echo '<div class="description">';
                                    echo $block_description;
                                echo '</div>';
                            }
                            if(!empty($button)){ echo '<a class="button" href="'.$button['url'].'">'.$button['title'].'</a>'; }
                        echo '</div>';
                    echo '</div>';
                echo '</div>';
                $item_count++;
            }

            // item
            echo '<div class="block-'.$item_count.' grid__item portable-one-half desk-one-'.$item_width.' desk-wide-one-'.$item_width.' collab_grid__item invisible">';
                echo '<div class="padder">';

                    // output image (with/without link)
                    if(!empty($link)) { echo '<div class="card-fade"><a class="collab_grid__item__image-link card" href="'.get_permalink($link).'">'; }
                        echo '<div class="front">'.wp_get_attachment_image($image['ID'], $item_width.'-thumb').'</div>';
                        echo '<div class="back" style="background-image: url('.wp_get_attachment_image_src($image['ID'], $item_width.'-thumb')[0].'); "><div><span>'.$link->post_title.'</span></div></div>';
                    if(!empty($link)) { echo '</a></div>'; }

                echo '</div>';
            echo '</div>'; // close item
        endwhile;

        echo '</div>'; // close items wrapper
    endif;

    if($close_container){ 
        echo '</div>'; // close container
        echo '</div>'; // close background container
    }
    
}


/**
 * Full width background with text overlay
 */
function cs_output_content_block__full_width_graphic_text_overlay($close_container){

    $background_color = get_sub_field('background_color');
    $background_image = get_sub_field('background_image');
    $background_repeat = get_sub_field('background_repeat');
    $background_height = get_sub_field('height');
    $background_type = get_sub_field('background_type');
    $background_video = get_sub_field('background_video');
    $content_width = get_sub_field('content_width');
    $content_alignment = get_sub_field('content_alignment');
    $content = get_sub_field('content');

    if($close_container){ 

        $background_url = wp_get_attachment_image_src($background_image["id"], "large")[0];
        
        // define background div styles
        $style = '';
        $style .= (!empty($background_color)) ? 'background-color: '.$background_color.';' : '';
        $style .= (!empty($background_image)) ? 'background-image: url('.$background_url.');' : '';
        $style .= (!empty($background_repeat)) ? 'background-repeat: repeat;' : 'background-size: cover;';
        if( !empty($background_height) ) {
            $style .= 'min-height: '.$background_height.';';
        } else {
            if(!empty($background_image)){
                $aspect_ratio = $background_image['sizes']['large-height']/$background_image['sizes']['large-width'];
                $vw_unit = $aspect_ratio*100;
                $style .= "min-height:". $vw_unit . "vw;";
            }
        }
        
        $class  = '';
        $class .= (!empty($background_color)) ? ' has-color' : '';
        $class .= (!empty($background_image)) ? ' has-image' : '';
        echo '<div class="background-container'.$class.'" style="'.$style.'">'; // open background container
    }

    // output background image as an actual image for mobile
    if(!empty($background_image) && !empty($background_type) && $background_type === 'image'){
        echo '<img class="mobile-image" src="'.$background_url.'">';
    }

    // add video if selected and we have arve installed, output the video
    if(shortcode_exists('arve') && !empty($background_type) && $background_type === 'video'){
        echo '<div class="video">';
            echo do_shortcode( '[arve url="'.$background_video.'"]');
        echo '</div>';
    }
    
    // output content block
    if(!empty($content)){
    
        // produce style to align content
        $content_style = '';
        switch($content_alignment){
            case 'left':
                $content_style .= "justify-content: flex-start;";
                break;
            case 'center':
                $content_style .= "justify-content: center;";
                break;
            case 'right':
                $content_style .= "justify-content: flex-end;";
                break;
        }

        echo '<div class="contain flex">';
            echo '<div class="grid flex" style="'.$content_style.'">';
                echo '<div class="grid__item desk-one-'.$content_width.' desk-wide-one-'.$content_width.' invisible" style="'.$content_style.'">';
                    echo $content;
                echo '</div>';
            echo '</div>';
        echo '</div>';
    }

    if($close_container){ 
        echo '</div>'; // close background container
    }

}


/**
 * Slider
 */
function cs_output_content_block__slider($close_container){

    $slider = get_sub_field('slider');
    if(!empty($slider) && function_exists('putRevSlider')) {
        putRevSlider($slider);
    }

}


/**
 * WYSIWYG Editor
 */
function cs_output_content_block__full_width_text_editor($close_container){
    echo '<div class="cf">'; 
        the_sub_field('wysiwyg'); 
    echo '</div>';
}


/**
 * Two Graphics
 */
function cs_output_content_block__two_graphics($close_container){

    // images
    $order = get_sub_field('order');
    $horizontal_image = get_sub_field('horizontal_image');
    $vertical_image = get_sub_field('vertical_image');

    // background settings
    $background_color = get_sub_field('background_color');
    $background_image = get_sub_field('background_image');
    $background_repeat = get_sub_field('background_repeat');

    if($close_container){ 
        
        // define background div styles
        $style = 'display:flex;';
        $style .= (!empty($background_color)) ? 'background-color: '.$background_color.';' : '';
        $style .= (!empty($background_image)) ? 'background-image: url('.wp_get_attachment_image_src($background_image["id"], "large")[0].');' : '';
        $style .= (!empty($background_repeat)) ? 'background-repeat: repeat;' : 'background-size: cover;';
        if( !empty($background_height) ) {
            $style .= 'min-height: '.$background_height.';';
        } else {
            if(!empty($background_image)){
                $aspect_ratio = $background_image['sizes']['large-height']/$background_image['sizes']['large-width'];
                $vw_unit = $aspect_ratio*100;
                $style .= "min-height:". $vw_unit . "vw;";
            }
        }
        
        $class  = '';
        $class .= (!empty($background_color)) ? ' has-color' : '';
        $class .= (!empty($background_image)) ? ' has-image' : '';
        echo '<div class="background-container'.$class.'" style="'.$style.'">'; // open background container
    }

    echo '<div class="images-container">';
        echo '<div class="images-item invisible">';
            if($order == 0) {
                echo wp_get_attachment_image($horizontal_image["id"], "large");
            } else {
                echo wp_get_attachment_image($vertical_image["id"], "large");
            }
            echo '</div>';
            echo '<div class="images-item">';
            if($order == 0) {
                echo wp_get_attachment_image($vertical_image["id"], "large");
            } else {
                echo wp_get_attachment_image($horizontal_image["id"], "large");
            }
        echo '</div>';
    echo '</div>';


    if($close_container){ 
        echo '</div>'; // close background container
    }

}


/**
 * Instagram
 */
function cs_output_content_block__instagram_feed($close_container){

    $instagram = get_sub_field('instagram');
    if(!empty($instagram) && shortcode_exists('ap_instagram_feed_pro')) {
        echo do_shortcode('[ap_instagram_feed_pro id="'.$instagram.'"]');
    }

}


/**
 * Horizontal Rule
 */
function cs_output_content_block__horizontal_rule($close_container){

    $color = get_sub_field('color');
    $spacing = get_sub_field('spacing');

    echo '<hr class="full-width" style="border-top-color: '.$color.'; margin-top: '.$spacing.'; margin-bottom: '.$spacing.';">';
}


/**
 * Features List
 */
function cs_output_content_block__features_list($close_container){

    if(have_rows('features')): 
        echo '<div class="features-list">';
            echo '<div class="grid">';
                while(have_rows('features')): the_row();

                    $icon = wp_get_attachment_image( get_sub_field('icon'), 'quarter-thumb');
                    $title = get_sub_field('title');
                    $description = get_sub_field('description');
                    $link = get_sub_field('link');

                    echo '<div class="grid__item lap-and-up-one-quarter feature">';
                        echo '<div class="feature__image">';
                            if(!empty($link)) { echo '<a href="'.$link.'">'; }
                            echo $icon;
                            if(!empty($link)) { echo '</a>'; }
                        echo '</div>';
                        if ( !empty($title) ) { 
                            if(!empty($link)) { echo '<a href="'.$link.'">'; }
                            echo '<h3 class="feature__title">'.$title.'</h3>'; 
                            if(!empty($link)) { echo '</a>'; }
                        }
                        if ( !empty($description) ) { echo '<div class="feature__description">'.$description.'</div>'; }
                    echo '</div>';
                endwhile;
            echo '</div>';
        echo '</div>';
    endif;
}


/**
 * Features List
 */
function cs_output_content_block__testimonials_list($close_container){

    if(have_rows('testimonials')): 
        echo '<div class="testimonials-list">';
            echo '<div class="grid">';
                while(have_rows('testimonials')): the_row();

                    $attribution = get_sub_field('attribution');
                    $quote = get_sub_field('quote');

                    echo '<div class="grid__item lap-and-up-one-third testimonial">';
                        if ( !empty($quote) ) { 
                            echo '<blockquote class="testimonial__quote">';
                            echo '<p>'.$quote.'</p>';
                            if ( !empty($attribution) ) { echo '<cite class="testimonial__attribution">'.$attribution.'</cite>'; }
                            echo '</blockquote>'; 
                        }
                    echo '</div>';
                endwhile;
            echo '</div>';
        echo '</div>';
    endif;
}


/**
 * Output the video row content block
 */
function cs_output_content_block__video_row($close_container){

    $background_color = get_sub_field('background_color');
    $background_image = get_sub_field('background_image');
    $background_repeat = get_sub_field('background_repeat');
    $block_title = get_sub_field('block_title');
    $block_description = get_sub_field('block_description');
    $block_layout = get_sub_field('format');
    $block_items = get_sub_field('items');

    if($close_container){ 
        
        // define background div styles
        $style = '';
        $style .= (!empty($background_color)) ? 'background-color: '.$background_color.';' : '';
        $style .= (!empty($background_image)) ? 'background-image: url('.wp_get_attachment_image_src($background_image["id"], "large")[0].');' : '';
        $style .= (!empty($background_repeat)) ? 'background-repeat: repeat;' : 'background-size: cover;';
        
        echo '<div class="background-container" style="'.$style.'">'; // open background container
        echo '<div class="contain">'; // open container div for content
    }

    // output the block title and description
    if(!empty($block_title)) { echo '<h2>'.$block_title.'</h2>'; }
    if(!empty($block_description)) { echo '<div class="description">'.$block_description.'</div>'; }

    // loop through the items
    if(have_rows('items')):

        $item_count = 0;

        echo '<div class="grid video-row__items">';

        while(have_rows('items')): the_row('items'); $item_count++;

            // get unconditional fields
            $type = get_sub_field('type');
            $image = get_sub_field('image');
            $aspect_ratio = $image['sizes']['large-height']/$image['sizes']['large-width']*100;
            $title = get_sub_field('title');
            $video = get_sub_field('video');

            // decide grid class based on $layout
            $class = 'block-'.$item_count.' grid__item ';
            switch($block_layout){
                case 'half':
                    $class .= "desk-one-half desk-wide-one-half";
                    break;
                case 'third':
                    $class .= "desk-one-third desk-wide-one-third";
                    break;
                case 'quarter':
                    $class .= "desk-one-quarter desk-wide-one-quarter";
                    break;
                case 'half-third': // if it's the first, second, 5n+1 or 5n+2
                    if ( $item_count == 1 || $item_count == 2 || $item_count % 5 == 1 || $item_count % 5 == 2 )
                        $class .= "desk-one-half desk-wide-one-half";
                    else 
                        $class .= "desk-one-third desk-wide-one-third";
                    break;
                case 'third-half': // if it's the fourth, fifth, 4n+1 or 4n+2
                    if ( $item_count == 4 || $item_count == 5 || $item_count % 5 == 0 || $item_count % 5 == 4 )
                        $class .= "desk-one-half desk-wide-one-half";
                    else 
                        $class .= "desk-one-third desk-wide-one-third";
                    break;
            }

            // item
            echo '<div class="'.$class.' video-row__item invisible animated fadeIn">';

                echo '<div class="video-row__item__video-container"><div class="embed-container">';
                    echo $video;
                echo '</div></div>';
                    
                // output title
                if(!empty($title)) { echo '<div class="video-row__item__meta"><span class="video-row__item__title">'.$title.'</span></div>'; }

            echo '</div>'; // close item
        endwhile;

        echo '</div>'; // close items wrapper
    endif;

    if($close_container){ 
        echo '</div>'; // close container
        echo '</div>'; // close background container
    }
    
}