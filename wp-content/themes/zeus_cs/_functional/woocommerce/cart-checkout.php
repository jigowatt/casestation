<?php

if(function_exists('cs_log')) {
  cs_log('LOADED _functional/woocommerce/cart-checkout.php');
}

/**
 * Make case designer products link to a thumbnail in the cart instead of the product
 */
if(!function_exists('cs_case_designer_products_link_to_thumbnail')) {
  add_filter( 'woocommerce_cart_item_thumbnail', 'cs_case_designer_products_link_to_thumbnail', 10, 3 );
  function cs_case_designer_products_link_to_thumbnail( $image, $cart_item, $cart_key )
  {
    if(function_exists('cs_log')) { cs_log('cs_case_designer_products_link_to_thumbnail()'); }
    if ( isset( $cart_item['ec3d'] )) return sprintf( '<a rel="prettyPhoto" data-rel="prettyPhoto" href="%s" target="_blank"><img src="%s" /></a>', $cart_item['ec3d']['thumburl'], $cart_item['ec3d']['thumburl'] );
    else return $image;
  }
}



/**
*  Move "newsletter subscription" checkbox from billing address area to terms area
*/
if ( class_exists( 'WC_Subscribe_To_Newsletter' ) && !function_exists('cs_move_checkout_newsletter_checkbox_to_terms')){
  add_action( 'init', 'cs_move_checkout_newsletter_checkbox_to_terms', 99 );
  function cs_move_checkout_newsletter_checkbox_to_terms()
  {
    if(function_exists('cs_log')) { cs_log('cs_move_checkout_newsletter_checkbox_to_terms()'); }
    remove_action( 'woocommerce_after_checkout_billing_form', array( $GLOBALS['WC_Subscribe_To_Newsletter'], 'newsletter_field' ), 5 );
    add_action( 'woocommerce_checkout_after_terms_and_conditions', array( $GLOBALS['WC_Subscribe_To_Newsletter'], 'newsletter_field' ), 5 );
  }
}



/**
* Remove order notes field
*/
if (!function_exists('cs_remove_order_notes_field')) {
  add_filter( 'woocommerce_checkout_fields' , 'cs_remove_order_notes_field' );
  function cs_remove_order_notes_field( $fields ) {
    if(function_exists('cs_log')) { cs_log('cs_remove_order_notes_field()'); }
    unset($fields['order']['order_comments']);
    return $fields;
  }
}
