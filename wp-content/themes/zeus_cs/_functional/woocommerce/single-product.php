<?php

if(function_exists('cs_log')) {
  cs_log('LOADED _functional/woocommerce/single-product.php');
}


/**
 * Add variation fields for…
 * - redirection to an ec3d product
 * - coming soon
 * - pre-order
 */
if(!function_exists('cs_add_variation_fields')){
  add_action( 'woocommerce_product_after_variable_attributes', 'cs_add_variation_fields' , 10, 3 );
  function cs_add_variation_fields( $loop, $variation_data, $variation )
  {
    if(function_exists('cs_log')) { cs_log('cs_add_variation_fields()'); }

    // EC3D Product reference
    woocommerce_wp_text_input(
      array(
        'id'          => 'woocommerce_ec3d_product_ref[' . $variation->ID . ']',
        'label'       => __( 'Case designer product slug:', 'zeus_cs' ),
        'desc_tip'    => 'true',
        'description' => __( 'Enter the slug for the personalise-IT product to link to.', 'woocommerce' ),
        'value'       => get_post_meta( $variation->ID, 'woocommerce_ec3d_product_ref', true )
      )
    );

    echo '<div style="clear:both">';

    // Coming soon
    woocommerce_wp_checkbox(
      array(
        'id'            => '_coming_soon',
        'wrapper_class' => 'checkbox',
        'label'         => __('Coming soon?', 'zeus_cs' ),
        'description'   => __( 'Shows "Coming soon" button (must be set to out-of-stock)', 'zeus_cs' ),
        'desc_tip'      => 'true',
        'value'         => get_post_meta( $variation->ID, '_coming_soon', true )
      )
    );

    echo '</div>';
    echo '<div style="clear:both">';

    // Pre-order text
    woocommerce_wp_text_input(
      array(
        'id'            => '_preorder',
        'wrapper_class' => '_preorder',
        'label'         => __('Pre-order Date', 'zeus_cs' ),
        'placeholder'   => __('May 4th', 'zeus_cs' ),
        'description'   => __( 'Shows pre-order button (must be in-stock)', 'zeus_cs' ),
        'desc_tip'      => 'true',
        'value'         => get_post_meta( $variation->ID, '_preorder', true )
      )
    );

    echo '</div>';
  }
}
if (!function_exists('cs_save_variation_fields')) {
  add_action( 'woocommerce_save_product_variation', 'cs_save_variation_fields', 10, 2 );
  function cs_save_variation_fields( $variation_id, $i )
  {
    if(function_exists('cs_log')) { cs_log('cs_save_variation_fields()'); }

    // EC3D Product reference
    $ec3d_prod_ref = isset( $_POST['woocommerce_ec3d_product_ref'][$variation_id] ) ? sanitize_title( $_POST['woocommerce_ec3d_product_ref'][$variation_id] ) : '';
  	update_post_meta( $variation_id, 'woocommerce_ec3d_product_ref', esc_attr( $ec3d_prod_ref ) );

    // Coming soon
    $soon = isset( $_POST['_coming_soon'] ) ? 'yes' : 'no';
  	update_post_meta( $variation_id, '_coming_soon', $soon );

    // Pre-order text
    $preorder = isset($_POST['_preorder']) ? $_POST['_preorder'] : '';
  	update_post_meta( $variation_id, '_preorder', esc_attr( $preorder ) );

  }
}


/**
 * Conditionally remove thumbnail images from the single product page
 * This is used where a product import for variable products adds lots of thumbnail images to the product.
 */
if (!function_exists('cs_woocommerce_remove_thumbnail_images')) {
  add_action('woocommerce_before_single_product', 'cs_woocommerce_remove_thumbnail_images');
  function cs_woocommerce_remove_thumbnail_images()
  {
    global $product;

    if(function_exists('cs_log')) { cs_log('cs_woocommerce_remove_thumbnail_images()'); }

    // get current product categories
    $cats = get_the_terms( $product->get_id(), 'product_cat' );
    $thumbcats = [];

    // check the product has categories
    if(ACF_ENABLED && !empty($cats)){

      // loop through top level categories and add to thumbcats array
      foreach ($cats as $cat) {
        if($cat->parent == 0){
          array_push($thumbcats, $cat);
        }
      }

      // loop through thumbcats array, check if the custom field for showing thumnails is set
      foreach($thumbcats as $cat){
        $hidethumbs = get_field('hide_thumbnails_in_single_product', $cat);
        if($hidethumbs){ remove_action( 'woocommerce_product_thumbnails', 'woocommerce_show_product_thumbnails', 20 ); }
      }
    }
  }
}


/**
 * Conditionally change the "Add to cart" button functionality on variable products
 * to redirect the user to a "case designer" EC3D product
 * - construct redirect url
 * - prevent addition to basket
 */
if(!function_exists('cs_add_to_cart_redirect')){
  add_action( 'woocommerce_add_to_cart_validation', 'cs_add_to_cart_redirect', 10, 5 );
  function cs_add_to_cart_redirect($true, $product_id = null, $quantity = null, $variation_id = null, $variation = null)
  {
    if(function_exists('cs_log')) { cs_log('cs_add_to_cart_redirect()'); }

    // get the product to validate
    $product = wc_get_product($product_id);
    $type    = $product->get_type();

    $ec3d_prod_ref = null;

    // get variable product EC3D redirect field value
    if ($type == 'variable' || $type == 'g3d_variable_stock_item') {
      $ec3d_prod_ref = get_post_meta( $variation_id, 'woocommerce_ec3d_product_ref', true );
    }

    // get all other product type's EC3D redirect field value
    else {
      $ec3d_prod_ref = get_post_meta( $product_id, 'woocommerce_ec3d_product_ref', true );
    }

    // if the field has a value, we need to change the add to cart button behaviour
    if(!empty($ec3d_prod_ref)) {

      // construct the redirect URL for "add to basket"
      $redirect_product = get_page_by_path( $ec3d_prod_ref, OBJECT, 'product' );
      $url = get_permalink($redirect_product);

      // redirect user if product exists, if not, let the user know it doesn't
      if($url && wp_redirect($url)){
        exit();
      } else {
        wc_add_notice( sprintf( __( "No case designer available for your choice", 'zeus_cs' ) ) ,'notice' );
        return false;
      }
    }


    // return true for other products
    return true;
  }
}


/**
 * Conditionally change the "Add to cart" button text
 */
if(!function_exists('cs_add_to_cart_button_text')){
  add_filter( 'woocommerce_product_single_add_to_cart_text', 'cs_add_to_cart_button_text' );
  function cs_add_to_cart_button_text() {
    global $post, $product;

    if(function_exists('cs_log')) { cs_log('cs_add_to_cart_button_text()'); }

    $layout = cs_get_product_category_layout($post->ID);
    $soon = get_post_meta( $post->ID, '_coming_soon', true );
    $preorder = get_post_meta( $post->ID, '_preorder', true );

    // check if our custom "coming soon" option is checked
    // @NOTE: This no longer works for Simple product types… WooCommerce hide the form if the product is out-of-stock or unpurchasable
    if ($product->get_type() === 'external') {
      return $product->button_text ? $product->button_text : __('Buy Product', 'woocommerce');
    }

    // check if our custom "coming soon" option is checked
    // @NOTE: This no longer works for Simple product types… WooCommerce hide the form if the product is out-of-stock or unpurchasable
    if ($soon && !$product->is_in_stock()) {
      return __( 'Coming soon', 'zeus_cs' );
    }

    // check if our custom "coming soon" option is checked
    if (!empty($preorder) && $product->is_in_stock()) {
      return __( 'Pre-order - Ships ', 'zeus_cs' ).$preorder;
    }

    // if it's a "brand" or "device" product
    if(in_array($layout, array('brand', 'device'))) {
      return __( 'Create now', 'zeus_cs' );
    }

    // default add to cart text
    return __( 'Add to basket', 'zeus_cs' );
  }
}


/**
 * Conditionally add a "Coming soon" button to simple products
 */
if (!function_exists('cs_add_coming_soon_button_to_single_products')) {
  add_action( 'woocommerce_single_product_summary', 'cs_add_coming_soon_button_to_single_products', 59 );
  function cs_add_coming_soon_button_to_single_products()
  {
    global $product;

    if(function_exists('cs_log')) { cs_log('cs_add_coming_soon_button_to_single_products()'); }

    // check product type before doing anything
    if ('simple' === $product->get_type() || 'g3d_stock_item' === $product->get_type()) {
      $soon = get_post_meta( $product->get_id(), '_coming_soon', true );
      if(!$product->is_in_stock() && $soon){
        echo '<button class="single_add_to_cart_button button alt" disabled>'.__('Coming soon', 'zeus_cs').'</button>';
      }
    }
  }
}


/**
 * Hide "out of stock" text
 */
if (!function_exists('cs_hide_out_of_stock_text')) {
  add_filter('woocommerce_get_stock_html', 'cs_hide_out_of_stock_text');
  function cs_hide_out_of_stock_text()
  {
    if(function_exists('cs_log')) { cs_log('cs_hide_out_of_stock_text()'); }
    return '';
  }
}


/**
 * Conditionally remove the quantity field from variable products
 */
if(!function_exists('cs_remove_variable_product_quantity_field')){
  add_filter( 'woocommerce_is_sold_individually', 'cs_remove_variable_product_quantity_field', 10, 2 );
  function cs_remove_variable_product_quantity_field( $return, $product ) {
    global $post;

    if(function_exists('cs_log')) { cs_log('cs_remove_variable_product_quantity_field()'); }

    $layout = cs_get_product_category_layout($post->ID);

    // if it's a "brand" product
    if(in_array($layout, array('brand', 'device'))) {
      return true;
    }
  }
}


/**
 * Add Amazon Pay below add to cart button
 */
if (!function_exists('cs_single_product_additional_badges')) {
  add_action( 'woocommerce_single_product_summary', 'cs_single_product_additional_badges', 60 );
  function cs_single_product_additional_badges()
  {
    global $post;

    if(function_exists('cs_log')) { cs_log('cs_single_product_additional_badges()'); }

    $layout = cs_get_product_category_layout($post->ID);

    if (ACF_ENABLED) {
      $amazon_pay_logo = get_field('amazon_pay_logo', 'option');
      if($amazon_pay_logo){
        echo '<p class="amazon-pay">';
          _e('Use Amazon Pay at checkout', 'zeus_cs');
          echo '<img src="'.$amazon_pay_logo.'" height="16" style="height:16px; vertical-align: bottom; margin-left: .5em;">';
        echo '</p>';
      }
    }
  }
}


/**
 * Add Free shipping notice below cart button
 */
if (!function_exists('cs_single_product_free_shipping_notice')) {
  add_action( 'woocommerce_single_product_summary', 'cs_single_product_free_shipping_notice', 61 );
  function cs_single_product_free_shipping_notice()
  {
    global $post;

    if(function_exists('cs_log')) { cs_log('cs_single_product_free_shipping_notice()'); }

    $shipping_text = (get_field('shipping_text', 'option')) ? get_field('shipping_text', 'option') : __('Free Shipping - SHIPPED SAME DAY', 'zeus_cs') ;

    echo '<div class="free-shipping">'.$shipping_text.'</div>';
  }
}


/**
 * Conditionally remove thumbnails from product gallery
 */
if (!function_exists('cs_remove_thumbnails')) {

  // remove gallery thumbnails from images area (to put in a tab)
  add_action( 'woocommerce_product_thumbnails', 'cs_remove_thumbnails', 19 );
  function cs_remove_thumbnails()
  {
    global $post;

    if(function_exists('cs_log')) { cs_log('cs_remove_thumbnails()'); }

    $layout = cs_get_product_category_layout($post->ID);

    // remove gallery thumbnails from images area (to put in a tab)
    if(in_array($layout, array('brand'))){
      remove_action( 'woocommerce_product_thumbnails', 'woocommerce_show_product_thumbnails', 20 );
    }
  }
}

/**
 * Rename product tabs
 */
if(!function_exists('cs_rename_product_tabs')){
  add_filter( 'woocommerce_product_tabs', 'cs_rename_product_tabs' );
  function cs_rename_product_tabs ( $tabs ) {
    global $product;
    
    // mess with description tab output
    $desc = $product->get_description();    
    if(!empty($desc)){
      $tabs['description']['title'] = __('Product Details', 'zeus_cs');
    }
    
    return $tabs;
  }
}

/**
 * Remove product tabs
 */
if(!function_exists('cs_remove_product_tabs')){
  add_filter( 'woocommerce_product_tabs', 'cs_remove_product_tabs' );
  function cs_remove_product_tabs ( $tabs ) {
      unset( $tabs['additional_information'] );
      unset( $tabs['reviews'] );
      return $tabs;
  }
}

add_filter( 'woocommerce_product_tabs', 'cs_reorder_product_tabs', 98 );
function cs_reorder_product_tabs( $tabs ) {
  global $product;
  $desc = $product->get_description();    
  if(!empty($desc)){
    $tabs['description']['priority'] = 1;			// Description first
  }

	return $tabs;
}

/**
 * 3 column product description where supported (content exists)
 */
if(!function_exists('cs_custom_description_tab') && !function_exists('cs_custom_description_tab_content')){
  add_filter( 'woocommerce_product_tabs', 'cs_custom_description_tab' );
  function cs_custom_description_tab( $tabs ) {
    global $product;

    // mess with description tab output
    $desc = $product->get_description();
    if(!empty($desc)){
      $tabs['description']['callback'] = 'cs_custom_description_tab_content';	// Custom description callback
    }
    
    // return tabs
    return $tabs;
  }

  function cs_custom_description_tab_content() {
    global $product;

    echo '<div class="grid widegutter">';
      if(ACF_ENABLED){
        $protection_level = get_field('protection_level', $product->get_id());
        if(!empty($protection_level)){
          echo '<div class="grid__item one-whole lap-and-up-five-twelfths">';
            echo $product->get_description();
          echo '</div>';
          echo '<div class="grid__item one-whole lap-and-up-four-twelfths">';
            echo '<h3>'.__('Protection level', 'zeus_cs').'</h3>';
            echo '<p class="protection-level"><img src="'.PATH_IMG.'protection_'.$protection_level.'.png"></p>';
          echo '</div>';
        } else {
          echo '<div class="grid__item one-whole lap-and-up-nine-twelfths">';
            echo $product->get_description();
          echo '</div>';  
        }
      }
      
      echo '<div class="grid__item one-whole lap-and-up-three-twelfths">';
        if(ACF_ENABLED){
          $layout = cs_get_product_category_layout($post->ID);          
          $cs_labs_lifetime_badge = get_field('cs_labs_lifetime_badge', 'option');
          if ($cs_labs_lifetime_badge && 'accessories' !== $layout) {
            echo '<p class="cs-labs-lifetime-badge">';
              echo '<img src="'.$cs_labs_lifetime_badge.'" alt="'.__('CS Labs Quality Tested | Made for life, lifetime guarantee').'">';
            echo '</p>';
          }
        }
      echo '</div>';
    echo '</div>';
  }
}

/**
 * Conditionally add product gallery tab
 */
if(!function_exists('cs_move_single_product_gallery_to_tab')){
  add_filter( 'woocommerce_product_tabs', 'cs_move_single_product_gallery_to_tab' );
  function cs_move_single_product_gallery_to_tab( $tabs = array() )
  {
    global $product, $post;

    if(function_exists('cs_log')) { cs_log('cs_move_single_product_gallery_to_tab()'); }

    // get category layout custom field
    $layout = cs_get_product_category_layout($post->ID);
    if($layout === 'brand'){

      // if we have thumbnails, register a new "gallery" tab
      $thumbs = $product->get_gallery_image_ids();
      if (!empty($thumbs)) {

        // define new tab
        $tabs['cs_gallery_tab'] = array(
      		'title' 	=> __( 'Gallery', 'zeus_cs' ),
      		'priority' 	=> 50,
      		'callback' 	=> 'cs_move_single_product_gallery_to_tab_content'
      	);

      }

      // remove additional information tab
      unset( $tabs['additional_information'] );
      unset( $tabs['reviews'] );

    }
    return $tabs;
  }
}
if(!function_exists('cs_move_single_product_gallery_to_tab_content')){
  function cs_move_single_product_gallery_to_tab_content()
  {
    global $product;

    if(function_exists('cs_log')) { cs_log('cs_move_single_product_gallery_to_tab_content()'); }

    // get thumbnails
    $thumbs = $product->get_gallery_image_ids();

    // output thumbnails in our grid structure
    echo '<div class="grid">';

    // loop through thumbs as columns
    foreach ($thumbs as $thumb) {
      $full_img = wp_get_attachment_image_src($thumb, 'full');
      $thumb_img = wp_get_attachment_image_src($thumb, 'shop_catalog');
      echo '<div class="grid__item one-half lap-one-third desk-one-sixth">';
        echo '<a href="'.$full_img[0].'" rel="prettyPhoto[post-gallery-'.$product->get_id().']" data-rel="prettyPhoto[post-gallery-'.$product->get_id().']"><img src="'.$thumb_img[0].'"></a>';
      echo '</div>';
    }
    echo '</div>';
  }
}

/**
 * Conditionally remove related products
 */
if (!function_exists('cs_remove_related_products')) {
  add_filter('woocommerce_after_single_product_summary','cs_remove_related_products', 10);
  function cs_remove_related_products( $args ) {
    global $post;

    if(function_exists('cs_log')) { cs_log('cs_remove_related_products()'); }

    $layout = cs_get_product_category_layout($post->ID);
    if($layout === 'brand' || $layout === 'device'){
      remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
    }
  }
}


/**
 * Add clearing div above tabs in product (cater for lack of tabs)
 */
if (!function_exists('cs_clear_single_product_floats_on_category_pages')) {
  add_action( 'woocommerce_after_single_product_summary', 'cs_clear_single_product_floats_on_category_pages', 9 );
  function cs_clear_single_product_floats_on_category_pages()
  {
    if(function_exists('cs_log')) { cs_log('cs_clear_single_product_floats_on_category_pages()'); }
    if(is_product_category()) { echo '<div class="cf"></div>'; }
  }
}


/**
* Insert "Add to Wishlist" icon
 * notes:
 * - makes use of yith wishlist plugin
 * - doesn't apply to case designer product (ec3d product type)
 * - doesn't apply to brand or collections sections
 */
if (!function_exists('cs_add_wishlist_icon_to_single_featured_image')) {
  add_action( 'woocommerce_before_single_product_summary', 'cs_add_wishlist_icon_to_single_featured_image', 10 );
  function cs_add_wishlist_icon_to_single_featured_image()
  {
    global $product, $post;

    if(function_exists('cs_log')) { cs_log('cs_add_wishlist_icon_to_single_featured_image()'); }

    $layout = cs_get_product_category_layout($post->ID);

    if (
      defined( 'YITH_WCWL' ) &&
      $product->get_type() !== 'ec3d'
    ) {
      echo do_shortcode('[yith_wcwl_add_to_wishlist]');
    }
  }
}


/**
 * Add custom "add to cart" button options to woo product general settings (non variable products)
 * - Coming soon text is used for out-of-stock items to prevent sales
 * - pre-order text is used for in-stock items which can't be shipped just yet
 */
if (!function_exists('cs_add_general_product_options')) {
  add_action( 'woocommerce_product_options_general_product_data', 'cs_add_general_product_options' );
  function cs_add_general_product_options()
  {
    global $woocommerce, $post;

    if(function_exists('cs_log')) { cs_log('cs_add_general_product_options()'); }


    $product = wc_get_product($post->ID);

    // add general fields only when not a variable product
    if ('variable' != $product->get_type()) {

      $types = wc_get_product_types();
      $class = '';
      foreach ($types as $type => $value) {
        if($type !== 'variable')
        $class .= ' show_if_'.$type;
      }

      echo "<div class=\"options_group{$class}\">";
        echo "<div style=\"clear:both;\">";
        woocommerce_wp_checkbox(
          array(
            'id'            => '_coming_soon',
            'wrapper_class' => 'checkbox'.$class,
            'label'         => __('Coming soon?', 'zeus_cs' ),
            'description'   => __( 'Display different "Add to cart" button text (out-of-stock items only)', 'zeus_cs' ),
            'desc_tip'      => 'true'
        	)
        );
        echo '</div>';
        echo "<div style=\"clear:both;\">";
        woocommerce_wp_text_input(
          array(
            'id'            => '_preorder',
            'wrapper_class' => '_preorder'.$class,
            'label'         => __('Pre-order?', 'zeus_cs' ),
            'placeholder'   => __('May 4th', 'zeus_cs' ),
            'description'   => __( 'Display different "Add to cart" button text (in-stock items only)', 'zeus_cs' ),
            'desc_tip'      => 'true'
        	)
        );
        echo '</div>';
        echo "<div style=\"clear:both;\">";
        woocommerce_wp_text_input(
          array(
            'id'            => 'woocommerce_ec3d_product_ref',
            'wrapper_class' => 'woocommerce_ec3d_product_ref'.$class,
            'label'         => __('Case designer product slug:', 'zeus_cs' ),
            'placeholder'   => __('case-designer-slug', 'zeus_cs' ),
            'description'   => __( 'Enter the slug for the personalise-IT product to link to.', 'zeus_cs' ),
            'desc_tip'      => 'true'
        	)
        );
        echo '</div>';
      echo '</div>';
    }

  }
}
if (!function_exists('cs_save_general_product_options')) {
  add_action( 'woocommerce_process_product_meta', 'cs_save_general_product_options' );
  function cs_save_general_product_options( $post_id )
  {
    $product = wc_get_product($post_id);

    // save data if we're not a variable product
    if('variable' != $product->get_type() ){
      if(function_exists('cs_log')) { cs_log('cs_save_general_product_options()'); }
      $ec3d_prod_ref = isset($_POST['woocommerce_ec3d_product_ref']) ? $_POST['woocommerce_ec3d_product_ref'] : '';
      update_post_meta( $post_id, 'woocommerce_ec3d_product_ref', $ec3d_prod_ref );
      $soon = isset( $_POST['_coming_soon'] ) ? 'yes' : 'no';
      update_post_meta( $post_id, '_coming_soon', $soon );
      $preorder = isset($_POST['_preorder']) ? $_POST['_preorder'] : '';
      update_post_meta( $post_id, '_preorder', $preorder );
    }
  }
}


/**
 * Correctly redirect users to the cart when adding a case-designer product
 */
if(!function_exists('cs_redirect_ec3d_add_to_cart')){
  add_filter( 'woocommerce_ec3d_cart_redirect_for_designer', 'cs_redirect_ec3d_add_to_cart' );
  function cs_redirect_ec3d_add_to_cart( $url )
  {
    if(function_exists('cs_log')) { cs_log('cs_redirect_ec3d_add_to_cart()'); }
    if ( isset( $_POST['ec3d_data'] ) ) { $url = wc_get_cart_url(); }
    return $url;
  }
}


/**
 * Shuffle the variable product summary elements around
 */
if (!function_exists('cs_shuffle_variable_product_summary')) {
  add_action('woocommerce_before_single_product', 'cs_shuffle_variable_product_summary');
  function cs_shuffle_variable_product_summary()
  {
    if(function_exists('cs_log')) { cs_log('cs_shuffle_variable_product_summary()'); }
    remove_action( 'woocommerce_single_variation', 'woocommerce_single_variation', 10 );
    add_action( 'woocommerce_before_variations_form', 'woocommerce_single_variation', 10 );
  }
}


/**
 * Conditionally add filter that removes the link from single product featured image
 */
if (!function_exists('cs_add_action_to_remove_link_from_single_product_featured_image')) {
  add_action('woocommerce_before_single_product', 'cs_add_action_to_remove_link_from_single_product_featured_image');
  function cs_add_action_to_remove_link_from_single_product_featured_image(){
    global $post;
    $layout = cs_get_product_category_layout($post->ID);
    if(in_array($layout, array('brand'))){
      add_filter('woocommerce_single_product_image_thumbnail_html', 'cs_remove_link_from_single_product_featured_image', 10, 2);
    }
  }
}
if (!function_exists('cs_remove_link_from_single_product_featured_image')) {
  function cs_remove_link_from_single_product_featured_image( $html, $post_id ) {
    if(function_exists('cs_log')) { cs_log('cs_remove_link_from_single_product_featured_image()'); }
  	$post_thumbnail_id = get_post_thumbnail_id( $post_id );
    return get_the_post_thumbnail( $post_thumbnail_id, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ) );
  }
}


/**
 * Hide price range on variable products
 * https://gist.github.com/jameskoster/c0cc71c71147ea66b8c2
 */
if(!function_exists('cs_hide_variable_product_price_range')){
  add_filter( 'woocommerce_variable_price_html', 'cs_hide_variable_product_price_range' );
  function cs_hide_variable_product_price_range( $price ) {
    if(function_exists('cs_log')) { cs_log('cs_hide_variable_product_price_range()'); }
  	global $product;

  	$product 	= new WC_Product_Variable( $product );
  	$min_price 	= $product->get_variation_price( 'min', true );
  	$max_price 	= $product->get_variation_price( 'max', true );

  	// Hide the range if the min/max price doesn't match
  	if ( $min_price !== $max_price ) {
  		$price = '';
  	}

  	return apply_filters( 'woocommerce_get_price_html', $price );
  }
}


/**
 * Add variation.meta_data back into the output of the single-product page
 */
if(!function_exists('cs_woocommerce_available_variation')){
  add_filter('woocommerce_available_variation', 'cs_woocommerce_available_variation', 10, 3);
  function cs_woocommerce_available_variation($variations, $product, $variation)
  {
      $metadata = $variation->get_meta_data();
      if (!empty($metadata)) {
          $variations = array_merge($variations, [
              'meta_data' => $metadata,
          ]);
      }

      return $variations;
  }
}

/**
 * Add "repeatable content blocks" to the single product pages
 */
if(!function_exists('cs_add_repeatable_content_blocks_to_single_product')){
  add_filter('woocommerce_after_single_product_summary', 'cs_add_repeatable_content_blocks_to_single_product');
  function cs_add_repeatable_content_blocks_to_single_product(){
    global $target, $post;
    $target = $post->ID;
    cs_content_blocks($target);
  }
}

/**
 * Add "case type"/"case style" css classes to products inside the device-cases category
 * This helps javascript to identify, hide/show and create a menu to switch which product is show in the category page.$_COOKIE
 */
if(!function_exists('cs_add_case_style_css_classes_to_device_category_page')){
  add_filter('post_class', 'cs_add_case_style_css_classes_to_device_category_page', 10,3);
  function cs_add_case_style_css_classes_to_device_category_page($classes){
    global $post;

    // get the category we're in
    $layout = cs_get_product_category_layout($post->ID);
    
    // check if it's in the device category
    if(!empty($layout) && in_array($layout, array('device'))){
    
      // get the type of case assigned to it
      $case_type = get_field('case_type', $post->ID);
    
      // if we have one assigned, add a couple of css classes to the product container
      // for javascript to have it's way with.
      if(!empty($case_type)){ $classes[] = 'case-type case-type--'.$case_type['value']; }
    }

    return $classes;
  }
}

/**
 * Add Anchor point for javascrtipt generated device case type menu to hook onto
 */
if(!function_exists('cs_add_case_type_anchor_to_device_case_product')){
  add_filter('woocommerce_before_single_product_summary', 'cs_add_case_type_anchor_to_device_case_product');
  function cs_add_case_type_anchor_to_device_case_product(){
    global $post;
    
    // get the category we're in
    $layout = cs_get_product_category_layout($post->ID);
    
    // check if it's in the device category
    if(!empty($layout) && in_array($layout, array('device'))){
    
      // get the type of case assigned to it
      $case_type = get_field('case_type', $post->ID);
    
      // if we have one assigned, add the anchor for javascript to have it's way with.
      if(!empty($case_type)){ echo '<a id="case-type-'.$case_type['value'].'" name="'.$case_type['value'].'" data-case-type-display-text="'.$case_type['label'].'"></a>'; }
    }
  }
}