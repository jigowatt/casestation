<?php

if(function_exists('cs_log')) {
  cs_log('LOADED _functional/woocommerce/woocommerce.php');
}

if(class_exists('WooCommerce')) {
  include_once PATH_PHP.'woocommerce/global.php';             // multiple shop page
  include_once PATH_PHP.'woocommerce/categories.php';         // category pages
  include_once PATH_PHP.'woocommerce/single-product.php';     // single product page
  include_once PATH_PHP.'woocommerce/cart-checkout.php';      // checkout process pages
}
