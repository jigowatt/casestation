<?php

if(function_exists('cs_log')) {
  cs_log('LOADED _functional/woocommerce/global.php');
}

/**
* Tell WooCommerce the theme supports it
*/
if(!function_exists('cs_woocommerce_support')){
  add_action( 'after_setup_theme', 'cs_woocommerce_support' );
  function cs_woocommerce_support() {
    if(function_exists('cs_log')) { cs_log('cs_woocommerce_support()'); }
    add_theme_support( 'woocommerce' );
    add_theme_support( 'wc-product-gallery-zoom' );
    add_theme_support( 'wc-product-gallery-lightbox' );
    add_theme_support( 'wc-product-gallery-slider' );
  }
}


/**
 * Disable plugins on staging sites (useful for live-only functionality)
 */
if(!function_exists('cs_disable_staging_site_plugins')){
  add_action( 'init',  'cs_disable_staging_site_plugins');
  function cs_disable_staging_site_plugins() {

    // disable only on staging sites
    if(IS_STAGING){

      // woocommerce google analytics
      if ( is_plugin_active('woocommerce-google-analytics-integration/woocommerce-google-analytics-integration.php') ) {
        deactivate_plugins('woocommerce-google-analytics-integration/woocommerce-google-analytics-integration.php');    
      }

    }
  } 
}


/**
* Remove woocommerce hooks
* - opening & closing Wrappers
* - sidebar
* - single-product meta (sku etc)
*/
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );
remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );



/**
 * Deregister select2 from woocommerce (conflicts with theme addition of select2)
 */
if(!function_exists('cs_woocommerce_deregister_select2')){
  add_action( 'wp_enqueue_scripts', 'cs_woocommerce_deregister_select2' );
  function cs_woocommerce_deregister_select2()
  {
    if(function_exists('cs_log')) { cs_log('cs_woocommerce_deregister_select2()'); }
    if ( class_exists( 'woocommerce' ) ) {
      wp_dequeue_style( 'select2' );
      wp_deregister_style( 'select2' );
      wp_dequeue_script( 'select2');
      wp_deregister_script('select2');
    }
  }
}


/**
 * Include woocommerce's lightbox on the rest of the site
 */
if(!function_exists('frontend_scripts_include_lightbox')){
  add_action( 'wp_enqueue_scripts', 'frontend_scripts_include_lightbox' );
  function frontend_scripts_include_lightbox() {
    global $woocommerce;

    if(function_exists('cs_log')) { cs_log('frontend_scripts_include_lightbox()'); }

    $suffix      = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
    $lightbox_en = get_option( 'woocommerce_enable_lightbox' ) == 'yes' ? true : false;

    if ( $lightbox_en ) {
      wp_enqueue_script( 'prettyPhoto', $woocommerce->plugin_url() . '/assets/js/prettyPhoto/jquery.prettyPhoto' . $suffix . '.js', array( 'jquery' ), null, true );
      wp_enqueue_script( 'prettyPhoto-init', $woocommerce->plugin_url() . '/assets/js/prettyPhoto/jquery.prettyPhoto.init' . $suffix . '.js', array( 'jquery' ), $woocommerce->version, true );
      wp_enqueue_style( 'woocommerce_prettyPhoto_css', $woocommerce->plugin_url() . '/assets/css/prettyPhoto.css' );
    }
  }
}


/**
 * Update the mini-cart via ajax when a new product is added to the cart
 */
if(!function_exists('cs_woocommerce_header_add_to_cart_fragment')){
  add_filter('woocommerce_add_to_cart_fragments', 'cs_woocommerce_header_add_to_cart_fragment');
  function cs_woocommerce_header_add_to_cart_fragment( $fragments ) {
  	global $woocommerce;

    if(function_exists('cs_log')) { cs_log('cs_woocommerce_header_add_to_cart_fragment()'); }

  	ob_start();
  ?>
    <div class="cs-mini-cart ajaxed">
      <a class="cart-icon" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e( 'View your shopping cart', 'zeus_cs' ); ?>">
        (<?php echo sprintf ( _n( '%d', '%d', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() ); ?>)
      </a>
      <div class="cs-mini-cart__details">
        <?php woocommerce_mini_cart(); ?>
      </div>
    </div>
  <?php
  	$fragments['.cs-mini-cart'] = ob_get_clean();
  	return $fragments;
  }
}


/**
 * Conditionally add a css class to the body for product categories based on the
 * "layout" custom field (accessories, brand, collections etc)
 * This allows us to style them differently without duplicating woo templates.
 */
if (!function_exists('cs_product_category_body_class')) {
  add_filter( 'body_class','cs_product_category_body_class' );
  function cs_product_category_body_class( $classes ) {
    global $wp_query, $post;

    if(function_exists('cs_log')) { cs_log('cs_product_category_body_class()'); }

    if (is_product_category()) {
      $cat = $wp_query->get_queried_object();
      $layout = cs_get_product_category_layout(null, $cat->term_id);
    }

    if(is_product()){
      $layout = cs_get_product_category_layout($post->ID);
    }

    if(!empty($layout)){
      $classes[] = 'cs-'.$layout.'-category';
    }

    return $classes;
  }
}


/**
 * Conditionally wrap woocommerce page content
 * useful for full-width hero blocks (see categories/single for hero blocks)
 */
if (!function_exists('cs_woocommerce_output_content_wrapper')){
  add_action( 'woocommerce_before_single_product_summary', 'cs_woocommerce_output_content_wrapper', 10 );
  add_action( 'woocommerce_after_single_product', 'cs_woocommerce_output_content_wrapper_end', 10 );
  add_action( 'woocommerce_before_shop_loop', 'cs_woocommerce_output_content_wrapper', 10 );
  function cs_woocommerce_output_content_wrapper()
  {
    global $wp_query, $post;

    if(function_exists('cs_log')) { cs_log('cs_woocommerce_output_content_wrapper()'); }

    $layout = null;

    if (is_product_category()) {
      $cat = $wp_query->get_queried_object();
      $layout = cs_get_product_category_layout(null, $cat->term_id);
    }

    if(is_product()){
      $layout = cs_get_product_category_layout($post->ID);
    }

    if(!empty($layout)){
      if(
        (is_product_category() && in_array($layout, array('accessories', 'brand', 'collections', 'device')))
      ){
        echo '<div class="contain" data-file="global">';
      }
    }
  }
}
if (!function_exists('cs_woocommerce_output_content_wrapper_end')){
  add_action( 'woocommerce_after_main_content', 'cs_woocommerce_output_content_wrapper_end', 10 );
  function cs_woocommerce_output_content_wrapper_end()
  {
    global $wp_query, $post;

    if(function_exists('cs_log')) { cs_log('cs_woocommerce_output_content_wrapper_end()'); }

    $layout = null;

    if (is_product_category()) {
      $cat = $wp_query->get_queried_object();
      $layout = cs_get_product_category_layout(null, $cat->term_id);
    }

    if(is_product()){
      $layout = cs_get_product_category_layout($post->ID);
    }

    if(!empty($layout)){
      if(
        (is_product_category() && in_array($layout, array('accessories', 'brand', 'collections', 'device')))
      ){
        echo '</div><!-- cs_woocommerce_output_content_wrapper_end -->';
      }
    }
  }
}


/**
 * Adding links to mini-cart for…
 * - wishlist
 * - orders
 * - account
 * - sign in/out
 */
if (!function_exists('cs_add_links_to_minicart')) {
  add_action('woocommerce_after_mini_cart', 'cs_add_links_to_minicart', 10);
  function cs_add_links_to_minicart()
  {
    global $current_user;
    $current_user = wp_get_current_user();

    if(function_exists('cs_log')) { cs_log('cs_add_links_to_minicart()'); }

    echo '<hr>';

    // wishlist
    if(class_exists('YITH_WCWL')){
      echo '<a href="'.YITH_WCWL()->get_wishlist_url( 'manage' ).'" class="mini-cart-link mini-cart-link--wishlist">'.__('Wishlist', 'zeus_cs').' ('.YITH_WCWL()->count_products().')</a>';
    }

    // Account links
    if( is_user_logged_in() ) {
      echo '<a href="'.wc_get_endpoint_url( 'orders', '', wc_get_page_permalink( 'myaccount' ) ).'" class="mini-cart-link mini-cart-link--orders">'.__('Orders', 'zeus_cs').'</a>';
      echo '<a href="'.wc_get_page_permalink( 'myaccount' ).'" class="mini-cart-link mini-cart-link--account">'.__('Account', 'zeus_cs').'</a>';
      echo '<a href="'.wc_get_endpoint_url( 'customer-logout', '', wc_get_page_permalink( 'myaccount' ) ).'" class="mini-cart-link mini-cart-link--user">'.__('Sign out', 'zeus_cs').' '.$current_user->user_firstname.'</a>';
    } else {
      echo '<a href="'.wc_get_endpoint_url( 'orders', '', wc_get_page_permalink( 'myaccount' ) ).'" class="mini-cart-link mini-cart-link--orders">'.__('Orders', 'zeus_cs').'</a>';
      echo '<a href="'.wc_get_page_permalink( 'myaccount' ).'" class="mini-cart-link mini-cart-link--account">'.__('Account', 'zeus_cs').'</a>';
      echo '<a href="'.wc_get_page_permalink( 'myaccount' ).'" class="mini-cart-link mini-cart-link--user">'.__('Sign in', 'zeus_cs').'</a>';
    }
  }
}


/**
 * Conditionally hide products from the main shop page
 * - certain products aren't purchasable (brand, collections, device layout products)
 * - ec3d "case designer" products shouldn't be accessible through the main shop page
 */
if (!function_exists('cs_exclude_products_from_shop_page')) {
  add_filter( 'woocommerce_product_query_tax_query', 'cs_exclude_products_from_shop_page', 10, 2 );
  function cs_exclude_products_from_shop_page($tax_query, $instance)
  {
    if(function_exists('cs_log')) { cs_log('cs_exclude_products_from_shop_page()'); }

    $excludes = array();

    // figure out if we should filter results
    if(
      !is_admin() &&       // it's a front-end page
      is_shop()         // it's the main /shop pages
    ){

      // get the product categories
      $product_cats = get_terms('product_cat');

      // loop through the categories and check the "layout" custom field,
      // add "brand", "collections" and "device" categories to the excludes list
      foreach ($product_cats as $cat) {
        $layout = get_field('layout', $cat->taxonomy.'_'.$cat->term_id);
        if (in_array($layout, array('brand', 'collections', 'device'))){
          array_push($excludes, $cat->slug);
          cs_log('EXCLUDE FROM /SHOP PAGE: '. $cat->slug);
        }
      }

      // change query to exclude case designer products and $excludes
      array_push(
        $tax_query,
        array(
          'relation' => 'AND',
          array(
            'taxonomy' => 'product_cat',
            'field'    => 'slug',
            'terms'    => $excludes,
            'operator' => 'NOT IN',
          ),
          array(
            'taxonomy' => 'product_type',
            'field'    => 'name',
            'terms'    => 'ec3d',
            'operator' => 'NOT IN',
          )
        )
      );

    }
    return $tax_query;
  }
}


/**
 *  add Google Customer Reviews Order Confirmation information to the WooCommerce Thank You page
 *  https://support.google.com/merchants/answer/7106244?hl=en
 */
if (!function_exists('cs_google_customer_reviews_order_confirmation')) {
  add_action( 'woocommerce_thankyou', 'cs_google_customer_reviews_order_confirmation', 99 );
  function cs_google_customer_reviews_order_confirmation( $order_id )
  {
    if(function_exists('cs_log')) { cs_log('cs_google_customer_reviews_order_confirmation()'); }

    // we only want this stuff to be present in live environments
    if(!IS_DEV){
      $order = new WC_Order( $order_id );
      $delivery_date = date('Y-m-d', strtotime('+6 day'));
    ?>

      <script src="https://apis.google.com/js/platform.js?onload=renderOptIn" async defer></script>

      <script type="text/javascript">
        window.renderOptIn = function() {
          window.gapi.load('surveyoptin', function() {
            window.gapi.surveyoptin.render(
            {
              "merchant_id": 113962980,
              "order_id": "<?php echo $order_id; ?>",
              "email": "<?php echo $order->billing_email; /* CUSTOMER_EMAIL */ ?>",
              "delivery_country": "<?php echo $order->billing_country; /* CUSTOMER_COUNTRY */ ?>",
              "estimated_delivery_date": "<?php echo $delivery_date; /* ORDER_EST_DELIVERY_DATE */ ?>"
            });
          });
        }
      </script>
    <?php
    }
  }
}

/**
 * Enable variation dropdowns to reduce options as user selects them
 * The default value is 10, we need 120 (tested product had 72 variations)
 */
if (!function_exists('cs_wc_ajax_variation_threshold')) {
  add_filter( 'woocommerce_ajax_variation_threshold', 'cs_wc_ajax_variation_threshold', 10, 2 );
  function cs_wc_ajax_variation_threshold( $qty, $product ) {
    if(function_exists('cs_log')) { cs_log('cs_wc_ajax_variation_threshold()'); }
  	return 120;
  }
}
