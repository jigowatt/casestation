<?php

if(function_exists('cs_log')) {
  cs_log('LOADED _functional/woocommerce/categories.php');
}


/**
 * Remove button from product listings
 */
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );


/**
 * Alter the number of columns for product archive pages
 * @return integer products per row
 */
if(!function_exists('cs_loop_columns')){
  add_filter( 'loop_shop_columns', 'cs_loop_columns' );
  function cs_loop_columns() {
    if(function_exists('cs_log')) { cs_log('cs_loop_columns()'); }
  	return apply_filters( 'cs_loop_columns', 4 ); /* 3 products per row */
  }
}


/**
 * Conditionally move/remove the breadcrumb on category pages
 */
if(!function_exists('cs_remove_product_category_breadcrumb')){
  add_action( 'woocommerce_before_main_content', 'cs_remove_product_category_breadcrumb' );
  function cs_remove_product_category_breadcrumb()
  {
    global $wp_query;
    if(function_exists('cs_log')) { cs_log('cs_remove_product_category_breadcrumb()'); }
    if(is_product_category()){
      $cat = $wp_query->get_queried_object();
      $layout = cs_get_product_category_layout(null, $cat->term_id);
      if($layout == 'accessories' || $layout = 'collections'){
        remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
      }
      if(in_array($layout, array('brand', 'device'))){
        remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
        add_action( 'woocommerce_before_shop_loop', 'woocommerce_breadcrumb', 20, 0 );
      }
    }
  }
}


/**
 * Swap out the result count for context based filtering
 * e.g - the artist on the collections page
 *     - device on the accessories page
 */
if(!function_exists('cs_woocommerce_widget_area')){
  remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
  add_action( 'woocommerce_before_shop_loop', 'cs_woocommerce_widget_area', 20 );
  function cs_woocommerce_widget_area()
  {
    if(function_exists('cs_log')) { cs_log('cs_woocommerce_widget_area()'); }
    $qobj = get_queried_object();
    if(!empty($qobj->term_id)){
      $layout = get_field('layout', 'product_cat_'.$qobj->term_id);
      if(!empty($layout) && $layout === 'collections'){
        echo '<div class="filter-container">';
        dynamic_sidebar( 'collections-filters-area' );
        echo '</div>';
      }
      if(!empty($layout) && $layout === 'accessories'){
        echo '<div class="filter-container">';
        dynamic_sidebar( 'accessories-filters-area' );
        echo '</div>';
      }
    }
  }
}


/**
 * Product category hero block and loop wrappers
 * - title
 * - description
 * - sharing icons
 * - sub-category tabs
 */
if (!function_exists('cs_product_category_hero_start')) {
  add_action( 'woocommerce_before_main_content', 'cs_product_category_hero_start', 10, 0 );
  function cs_product_category_hero_start()
  {
    global $wp_query;

    if(function_exists('cs_log')) { cs_log('cs_product_category_hero_start()'); }

    $cat = $wp_query->get_queried_object();

    if(is_product_category()){
      echo '<header class="woocommerce-products-header">';
    }

    if(ACF_ENABLED && is_product_category()){
      $cat = $wp_query->get_queried_object();
      $layout = cs_get_product_category_layout(null, $cat->term_id);
      $hero_enabled = get_field('show_hero_block', $cat);
      if( ($hero_enabled || is_null($hero_enabled)) && in_array($layout, array('accessories', 'brand', 'collections', 'device'))){
        
        // get category thumbnail image
        $image_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
        $image = wp_get_attachment_image_src( $image_id, 'full' );
        
        // get text colour if one is assigned
        $color = get_field('text_colour', $cat);
        $color = (!empty($color)) ? 'color: '.$color.';' : '';
        
        // get hero height if one is assigned
        $height = get_field('hero_block_height', $cat);
        $height = (!empty($height)) ? 'height: '.$height.'px !important;max-height: '.$height.'px !important;' : '';
        
        echo '<div class="product-cat-header product-cat-header--accessories">';
        echo   '<div class="contain">';
        echo     '<div class="background" style="background-image: url('.$image[0].'); '.$color.$height.'">';
      }
    }
  }
}
if(!function_exists('cs_remove_category_description_if_hero_block_hidden')){
  add_action('woocommerce_before_main_content', 'cs_remove_category_description_if_hero_block_hidden', 9);
  function cs_remove_category_description_if_hero_block_hidden (){
    global $wp_query;
    $cat = $wp_query->get_queried_object();
    
    if(ACF_ENABLED && is_product_category()){
      $hero_enabled = get_field('show_hero_block', $cat);
      if(!is_null($hero_enabled) && !$hero_enabled){
        remove_action( 'woocommerce_archive_description', 'woocommerce_taxonomy_archive_description', 10 );
        add_filter( 'woocommerce_show_page_title', function(){ return false; } );
      }
    }
  }
}
if (!function_exists('cs_product_category_hero_end')) {
  add_action( 'woocommerce_archive_description', 'cs_product_category_hero_end', 30 );
  function cs_product_category_hero_end()
  {
    global $wp_query;
    $parentcat;
    $tab_state = '';
    
    if(function_exists('cs_log')) { cs_log('cs_product_category_hero_end()'); }
    
    if(is_product_category()){
      
      // get currently displayed category
      $cat = get_term_by( 'slug', get_query_var('term'), get_query_var('taxonomy') );
      
      if ($cat) {
        
        // if current category IS the parent category…
        // - grab the children categories
        // - set the "state" for the parent categories tab to active
        if($cat->parent === 0){
          $parentcat = $cat;
          $subcats = get_categories( array ('taxonomy' => 'product_cat', 'parent' => $cat->term_id ));
          $tab_state = ' active';
        }
        
        // otherwise, the current category is a child and we need to get the parent category
        else {
          $parentcat = get_term($cat->parent, 'product_cat');
          $subcats = get_categories( array ('taxonomy' => 'product_cat', 'parent' => $cat->parent ));
        }
        
        // if advanced custom fields is active
        if(ACF_ENABLED){
          
          // get the catgory layout custom field value
          $cat = $wp_query->get_queried_object();
          $layout = cs_get_product_category_layout(null, $cat->term_id);
          
          // check which layout we're in and do stuff
          $hero_enabled = get_field('show_hero_block', $cat);
          if( ($hero_enabled || is_null($hero_enabled)) && in_array($layout, array('accessories', 'brand', 'collections', 'device'))){
    
            // check if we need dark or light social sharing icons and output them accordingly
            $dark_icons = get_field('dark_sharing_icons', $cat);
            if(!$dark_icons){ $dark_icons = ''; }
            cs_social_sharing_links($dark_icons);

            echo     '</div>';// .background

            // category tabs
            if($layout == 'accessories'){
              echo '<div class="nav-tabs-container">';
              echo   '<select class="nav-tabs-select" onchange="window.location.href = this.options[this.selectedIndex].value;">';
              foreach($subcats as $subcat){
                $selected = ($cat->term_id == $subcat->term_id) ? ' selected' : '';
                echo   '<option value="'.get_term_link($subcat->term_id).'"'.$selected.'>'.$subcat->name.'</option>';
              }
              echo   '</select>';
              echo   '<ul class="nav-tabs">';
              echo     '<li class="nav-tab'.$tab_state.'"><a href="'.get_term_link($parentcat->term_id).'">'.__('All', 'zeus_cs').' '.$parentcat->name.'</a></li>';
              foreach($subcats as $subcat){

                // check if current tab is active
                $tab_state = '';
                if($cat->term_id == $subcat->term_id){ $tab_state = ' active'; }

                // output tab
                echo   '<li class="nav-tab'.$tab_state.'"><a href="'.get_term_link($subcat->term_id).'">'.$subcat->name.'</a></li>';
              }
              echo   '</ul>';
              echo '</div>'; // .tabs-container
            }

            echo   '</div>'; // .contain
            echo '</div>'; // .product-cat-header
          }
        }
      }

      echo '</header>';
    }
  }
}

/**
 * Add repeatable content blocks to category page between hero block and content
 */
if(!function_exists('cs_add_repeatable_content_blocks_to_product_category')){
  add_filter('woocommerce_before_main_content', 'cs_add_repeatable_content_blocks_to_product_category', 1);
  function cs_add_repeatable_content_blocks_to_product_category(){
    global $target;
    $qobj = get_queried_object();
    if(isset($qobj->taxonomy) && isset($qobj->term_id)){
      $target = $qobj->taxonomy.'_'.$qobj->term_id;
      echo '<div class="contain">';
      cs_content_blocks($target);
      echo '</div>';
    }
  }
}


/**
 * Remove ordering dropdown on "Brand" category
 */
if (!function_exists('cs_remove_brand_category_ordering_dropdown')) {
  add_action( 'woocommerce_before_shop_loop', 'cs_remove_brand_category_ordering_dropdown', 29 );
  function cs_remove_brand_category_ordering_dropdown(){
    global $wp_query;

    if(function_exists('cs_log')) { cs_log('cs_remove_brand_category_ordering_dropdown()'); }

    if(is_product_category()){
      $cat = $wp_query->get_queried_object();
      $layout = cs_get_product_category_layout(null, $cat->term_id);
      if(in_array($layout, array('brand', 'device'))){
        remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
      }
    }
  }
}


/**
 * Conditionally load single-product.js on category pages (where the full product is pulled in)
 */
if(!function_exists('cs_load_single_product_js_on_category_page')){
  add_action( 'wp_enqueue_scripts', 'cs_load_single_product_js_on_category_page', 99 );
  function cs_load_single_product_js_on_category_page() {
    global $wp_query;

    if(function_exists('cs_log')) { cs_log('cs_load_single_product_js_on_category_page()'); }

    //enqueue script for brand category layout
    if(is_product_category()){
      $cat = $wp_query->get_queried_object();
      $layout = cs_get_product_category_layout(null, $cat->term_id);
      if (in_array($layout, array('brand', 'device'))) {
        wp_enqueue_script( 'wc-single-product' );
      }
      if ('device' === $layout) {
        if ( current_theme_supports( 'wc-product-gallery-zoom' ) ) {
          wp_enqueue_script( 'zoom' );
        }
        // if ( current_theme_supports( 'wc-product-gallery-slider' ) ) {
        //   wp_enqueue_script( 'flexslider' );
        // }
        if ( current_theme_supports( 'wc-product-gallery-lightbox' ) ) {
          wp_enqueue_script( 'photoswipe-ui-default' );
          wp_enqueue_style( 'photoswipe-default-skin' );
          add_action( 'wp_footer', 'woocommerce_photoswipe' );
        }
        wp_enqueue_script( 'wc-single-product' );
      }
    }
  }
}


/**
 * Remove related products from the "brand" category layout pages
 */
if(!function_exists('cs_brand_category_remove_related_products')){
  add_filter('woocommerce_related_products_args','cs_brand_category_remove_related_products', 10);
  function cs_brand_category_remove_related_products( $args ){
    global $wp_query;

    if(function_exists('cs_log')) { cs_log('cs_brand_category_remove_related_products()'); }

    if(is_product_category()){
      $cat = $wp_query->get_queried_object();
      $layout = cs_get_product_category_layout(null, $cat->term_id);
      if (in_array($layout, array('brand', 'device'))) {
        return array();
      }
    }
  }
}


/**
 * Limit WC Product Categories Widget on Collections category page to the
 * collections sub-categories (artists/collections)
 */
if (!function_exists('cs_limit_collections_category_select_list')) {
  add_filter('woocommerce_product_categories_widget_dropdown_args', 'cs_limit_collections_category_select_list');
  function cs_limit_collections_category_select_list($dropdown_args)
  {
    global $wp_query;

    if(function_exists('cs_log')) { cs_log('cs_limit_collections_category_select_list()'); }

    // check if we're on a collections category layout
    $cat = $wp_query->get_queried_object();
    $layout = cs_get_product_category_layout(null, $cat->term_id);
    if ('collections' === $layout) {

      $term = $wp_query->get_queried_object(); // grab the current category
      $child_of = array(); // store parent category IDs here

      // if we have a parent category
      if ($term->parent !== 0) {

        // get the parents, loop over them, add their IDs to the list
        $parents = get_ancestors($term->term_id, 'product_cat');
        foreach ($parents as $parent) { array_push($child_of, $parent); }

      }

      // otherwise, add the currently looped category ID to the list
      else { array_push($child_of, $term->term_id); }

      // modify the dropdown categories
      $dropdown_args['child_of'] = $child_of[0];
    }

    return $dropdown_args;
  }
}


/**
* Insert "Add to Wishlist" icon
* notes:
* - makes use of yith wishlist plugin
* - doesn't apply to case designer product (ec3d product type)
* - doesn't apply to brand or collections sections
 */
if (!function_exists('cs_add_wishlist_icon_to_category_thumbnails')) {
  add_action( 'woocommerce_before_shop_loop_item_title', 'cs_add_wishlist_icon_to_category_thumbnails', 10 );
  function cs_add_wishlist_icon_to_category_thumbnails()
  {
    global $product;

    if(function_exists('cs_log')) { cs_log('cs_add_wishlist_icon_to_category_thumbnails()'); }

    $layout = cs_get_product_category_layout($product->get_id());

    if (
      defined( 'YITH_WCWL' ) &&
      $product->get_type() !== 'ec3d'
    ) {
      echo do_shortcode('[yith_wcwl_add_to_wishlist]');
    }
  }
}


/**
 * Add search to collections products
 */
if (!function_exists('cs_add_collections_search_to_collections_category_pages')) {
  add_action( 'woocommerce_before_shop_loop', 'cs_add_collections_search_to_collections_category_pages', 10 );
  function cs_add_collections_search_to_collections_category_pages()
  {
    global $wp_query;
    if(function_exists('cs_log')) { cs_log('cs_add_collections_search_to_collections_category_pages()'); }

    // check if we're on a collections category layout
    $cat = $wp_query->get_queried_object();
    if(isset($cat->term_id)){
      $layout = cs_get_product_category_layout(null, $cat->term_id);
      if ('collections' == $layout) {
        ?>
        <form role="search" method="get" id="searchform" class="collections-searchform" action="<?php echo get_term_link($cat->term_id); ?>">
          <input type="text" placeholder="<? _e('Search for art', 'zeus_cs'); ?>" value="<?php echo get_search_query(); ?>" name="s" id="s" />
          <input type="submit" id="searchsubmit" class="screen-reader-text" value="<?php echo esc_attr_x( 'Search', 'submit button' ); ?>" />
        </form>
        <?php
      }
    }
  }
}


/**
 * Add javascript after device case type products to generate a tertiary menu and show/hide products
 */
if(!function_exists('cs_add_device_case_type_menu_javascript')){
  add_action('wp_footer', 'cs_add_device_case_type_menu_javascript');
  function cs_add_device_case_type_menu_javascript(){
    ?>
    <!-- Device sub-menu creation and behaviour -->
    <script>
    (function($){
      // provide case-type submenu and show relevant case type on device pages
      if ( $('[data-case-type-display-text]').length > 1){

        $('.product-cat-header').css('margin-top', '0');

        // add menu containers to the page
        $('.page-wrapper').prepend('<div class="case-type-menu-container"><a class="device-mobile-menu-trigger"><span><?php _e("Select another case style", "zeus_cs"); ?></span></a><ul class="case-type-menu contain"></ul></div>');

        //add click event handler to mobile menu trigger
        $('.device-mobile-menu-trigger').on('click', function(){
          $('.case-type-menu-container ul').slideToggle('fast');
        });


        // loop through each product anchor and add it as a link to the menu
        $('[data-case-type-display-text]').each(function(){
          const anchor = $(this);
          const menu = $('.case-type-menu');

          // add menu item to menu
          $('<li><a class="case-type-link" href="#'+anchor.attr('id')+'" data-case-type="'+anchor.attr('name')+'">'+anchor.data('case-type-display-text')+'</a></li>').appendTo(menu);
        });

        // hide/link to product on full page load
        $(window).on('load', function(){
          // show only the first item by default
          if(!window.location.hash){
            // $('.product.case-type').fadeOut(0).first().fadeIn('fast');
            $('.case-type-link').first().addClass('active');
          }

          // if a location hash is found process and show the related product
          else {
            const linkhash = window.location.hash.split('case-type')[1];
            // $('.product.case-type').fadeOut(0);
            // $('.product.case-type-'+linkhash).fadeIn('fast');

            $('.case-type-link').removeClass('active');
            $('.case-type-link[href*='+linkhash+']').addClass('active');

          }

          // prepend currently selected menu item text to trigger button
          $('.device-mobile-menu-trigger').prepend('<span class="current-case">'+$('.case-type-link.active').text()+'</span> – ');

          // loop through menu items, assign click event handler to links
          $('.case-type-link').click(function(e){
            e.preventDefault();

            // set link status
            $('.case-type-link').removeClass('active');
            $(this).addClass('active');

            // show correct data panel
            // $('.product.case-type').fadeOut(0); //hide all products
            // $('.product.case-type--'+$(this).data('case-type')).fadeIn('fast'); //show the clicked menu item's product

            // add correct text to menu trigger
            $('.device-mobile-menu-trigger .current-case').text($('.case-type-link.active').text());

            // animate scroll to data panel
            $('html, body').stop().delay(300).animate({
              scrollTop: $('a#case-type-'+$(this).data('case-type')).offset().top - 150
            }, 1000);
          });
        });

        function show_hide_mobile_device_menu()
        {
          if(window.innerWidth < 768){
            $('.case-type-menu-container').addClass('mobile-enabled');
            $('.case-type-menu-container ul').slideUp(0);
            $('.case-type-link').bind('click.menuSlide', function(e){ $('.case-type-menu-container ul').slideUp(0); });
          } else {
            $('.case-type-menu-container ul').removeAttr('style'); 
            $('.case-type-menu-container').removeClass('mobile-enabled');
            $('.case-type-link').unbind('click.menuSlide');
          }
        }show_hide_mobile_device_menu();

        $(window).on('resize', function(){
          show_hide_mobile_device_menu();
        });

      }
    })(jQuery);
    </script>
    <?php
  }
}