<?php

if(function_exists('cs_log')) {
  cs_log('LOADED _functional/widgets.php');
}

/**
 * Register widget areas
 */
register_sidebar( array(
	'name'          => __( 'Main sidebar', 'zeus_cs' ),
	'id'            => 'main-sidebar',
	'description'   => '',
	'class'         => '',
	'before_widget' => '<section id="%1$s" class="widget %2$s">',
	'after_widget'  => '</section>',
	'before_title'  => '<h2 class="widget-title">',
	'after_title'   => '</h2>'
) );

register_sidebar( array(
	'name'          => __( 'Mobile Menu Area', 'zeus_cs' ),
	'id'            => 'mobile-menu-area',
	'description'   => 'Mobile menu area (shows when menu button is clicked)',
	'class'         => '',
	'before_widget' => '',
	'after_widget'  => '',
	'before_title'  => '<strong class="widget-title">',
	'after_title'   => '</strong>'
) );

register_sidebar( array(
	'name'          => __( 'Collections Filters', 'zeus_cs' ),
	'id'            => 'collections-filters-area',
	'description'   => 'Additional collections navigation',
	'class'         => '',
	'before_widget' => '<div id="%1$s" class="woocommerce-filters %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h2 class="widget-title">',
	'after_title'   => '</h2>'
) );

register_sidebar( array(
	'name'          => __( 'Accessories Filters', 'zeus_cs' ),
	'id'            => 'accessories-filters-area',
	'description'   => 'Additional accessories navigation',
	'class'         => '',
	'before_widget' => '<div id="%1$s" class="woocommerce-filters %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h2 class="widget-title">',
	'after_title'   => '</h2>'
) );

register_sidebar( array(
	'name'          => __( 'Footer Newsletter', 'zeus_cs' ),
	'id'            => 'footer-newsletter',
	'description'   => 'Footer newsletter',
	'class'         => '',
	'before_widget' => '<div id="%1$s" class="footer-newsletter %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h2 class="widget-title">',
	'after_title'   => '</h2>'
) );

register_sidebar( array(
	'name'          => __( 'Footer Links', 'zeus_cs' ),
	'id'            => 'footer-links',
	'description'   => 'Footer links',
	'class'         => '',
	'before_widget' => '<div id="%1$s" class="footer-links %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h2 class="widget-title">',
	'after_title'   => '</h2>'
) );

register_sidebar( array(
	'name'          => __( 'Footer Text', 'zeus_cs' ),
	'id'            => 'footer-text',
	'description'   => 'Footer text',
	'class'         => '',
	'before_widget' => '<div id="%1$s" class="footer-text %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h2 class="widget-title">',
	'after_title'   => '</h2>'
) );


?>
