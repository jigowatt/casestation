<?php
// THEME VERSION (also used for cache busting)
$child_theme_version = '1.0.0';


// Force development mode (useful for forcing cache refresh)
// @TODO turn off before production deployment
define( 'IS_DEV', false );

// detect staging site based on WPEngine URL
if( strpos($_SERVER['SERVER_NAME'], '.staging.') || strpos($_SERVER['SERVER_NAME'], '.wpengine.') ) {
  define( 'IS_STAGING', true);
} else {
  define( 'IS_STAGING', false);
}

if (IS_DEV || IS_STAGING) {
	define( 'CHILD_VERSION', $child_theme_version.'--dev-'.date('U') );
} else {
	define( 'CHILD_VERSION', $child_theme_version ); // @NOTE: should match the style.css version #
}

/**
 * Enqueue child theme's CSS
 */
add_action( 'wp_enqueue_scripts', 'cs_add_child_theme_css' );
function cs_add_child_theme_css() {
    wp_enqueue_style( 'child', get_stylesheet_directory_uri().'/style.css', array('main'), CHILD_VERSION );
}

/**
 * Dashboard theme version number display
 */
if (!function_exists('cs_output_theme_version_in_dashboard')){
	add_filter( 'update_right_now_text', 'cs_output_theme_version_in_dashboard' );
	function cs_output_theme_version_in_dashboard( $content ){
		$content = str_replace('%2$s', '%2$s '.CHILD_VERSION.' (⚡️ Zeus '.VERSION.')', $content);
		return $content;
	}
}