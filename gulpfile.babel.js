import del from 'del';
import rename from 'gulp-rename';
import gulp from 'gulp';
import rtlcss from 'gulp-rtlcss';
import sass from 'gulp-sass';
import prefix from 'gulp-autoprefixer';
import imagemin from 'gulp-imagemin';
import webpack from 'webpack-stream';

// change depending on which gulp task is run
let THEME_BASE = '';

// list of theme folders
const themes = [
	'zeus_cs',
	'apollo_cs',
	'apollo_cs_jp'
];

// clear out previous compilations
export const cleanJS = () => {
	return del([THEME_BASE+'/dist/js/']);
}
export const cleanCSS = () => {
	return del([THEME_BASE+'/style.css']);
}
export const clean = () => {
	return del([
		THEME_BASE+'/style-rtl.css',
		THEME_BASE+'/style.css',
		THEME_BASE+'/dist',
	]);
}
gulp.task('clean', clean);

// styles
sass.compiler = require('dart-sass');
export function styles() {
	const sassConfig = {
		outputStyle: 'compressed',
		sourceMap: true,
	};
	const prefixerConfig = {
		cascade: false,
	};
	return gulp.src(THEME_BASE+'/src/scss/style.scss')
		.pipe(sass(sassConfig).on('error', sass.logError))
        .pipe(prefix(prefixerConfig))
        .pipe(gulp.dest(THEME_BASE)) // dump to theme root
        .pipe(rtlcss())
        .pipe(rename({ suffix: '-rtl' }))
        .pipe(gulp.dest(THEME_BASE)); // dump to theme root after RTL conversion
}

// scripts
export function scripts() {
  	return gulp.src(THEME_BASE+'/src/js/theme.js')
		.pipe(webpack({
            mode: 'production',
            target: 'web',
            output: {
                filename: 'theme.js',
            },
            module: {
                rules: [{
                test: /\.js$/,
                }],
		    },
		}))
		.pipe(gulp.dest(THEME_BASE+'/dist/js/'));
}
export function copyLibsToDist() {
    return gulp.src(THEME_BASE+'/src/js/lib/**/*.js')
        .pipe(gulp.dest(THEME_BASE+'/dist/js/lib/'));
}

// images
export function images() {
    return gulp.src(THEME_BASE+'/src/img/**/*.*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}]
        }))
        .pipe(gulp.dest(THEME_BASE+'/dist/img'));
}

// watch for changes
function watchFiles() {
	gulp.watch(THEME_BASE+'/src/**/*.js', gulp.series(cleanJS, scripts, copyLibsToDist));
	gulp.watch(THEME_BASE+'/src/**/*.scss', gulp.series(cleanCSS, styles));
}
export { watchFiles as watch };

// variable to run each task in series when required
const runSeries = gulp.series(clean, gulp.parallel(styles, scripts, copyLibsToDist, images, watchFiles));

// default task
gulp.task('guide', guide);
export const guide = function() {
	console.log('\n\n\n');
	console.log('====================================================================\n');
	console.log('Please use one of the following commands to compile each theme:\n');
	themes.forEach(theme => {
		console.log( '  gulp ' + theme );
	});
	console.log('\n==================================================================');
	console.log('\n\n\n');
	
	return Promise.resolve(''); // stops error showing in console
}

// loop through themes
themes.forEach(theme => {
	(function(theme){
		gulp.task(theme, function(){
			THEME_BASE = './wp-content/themes/'+theme;
			runSeries();
		});
	})(theme);
});


/*
 * Export a default task
 */
export default guide;
